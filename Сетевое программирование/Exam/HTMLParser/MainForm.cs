﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;

using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace HTMLParser
{
    public partial class MainForm : Form
    {
        string outputPath = string.Empty;
        public MainForm()
        {
            InitializeComponent();
            Initialize();
        }
        private void Initialize() 
        {
            textBoxUrl.Text = "http://barabash.com";
        }
        private void buttonOpenFolderBrowser_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBoxSavePath.Text = folderBrowserDialog1.SelectedPath;
                outputPath = textBoxSavePath.Text+"\\links.txt";
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            buttonSave.Enabled = false;
            if (string.IsNullOrEmpty(textBoxUrl.Text))
                return;

            string url = textBoxUrl.Text;
            Thread parseThread = new Thread(new ParameterizedThreadStart(DoWork));
            parseThread.IsBackground = true;
            parseThread.Start(url);
        }
        private void DoWork(object url) 
        {
            StringBuilder builder = new StringBuilder();
            List<string> levelOneCollection = new List<string>();
            levelOneCollection = (List<string>)GetHttpLinksList((string)url);

            progressBar.BeginInvoke((MethodInvoker)(() =>
            progressBar.Maximum = levelOneCollection.Count));

            foreach (var link in levelOneCollection) 
            {
                builder.Append($"{link}\n");
                var subCollection = GetHttpLinksList(link);
                foreach (var subItem in subCollection)
                    builder.Append($"- - - -{subItem}\n");

                progressBar.BeginInvoke((MethodInvoker)(() =>
                progressBar.Value++));
            }

            File.WriteAllText(outputPath, builder.ToString());

            buttonSave.BeginInvoke((MethodInvoker)(() =>
            buttonSave.Enabled = true));

            MessageBox.Show("Ссылки сохранены");
        }
        private IEnumerable<string> GetHttpLinksList(string url) 
        {
            List<string> collection = new List<string>();
            try
            {
                var doc = new HtmlWeb().Load(url);
                var linkTags = doc.DocumentNode.Descendants("link");
                var linkedPages = doc.DocumentNode.Descendants("a")
                                                  .Select(a => a.GetAttributeValue("href", null))
                                                  .Where(u => !String.IsNullOrEmpty(u));

                foreach (var str in linkedPages)
                    collection.Add(str);
            }
            catch{ }


            return collection;
        }
    }
}



//private void DoWork(object url)
//{
//    string data = string.Empty;
//    HttpWebRequest request = (HttpWebRequest)WebRequest.Create((string)url);
//    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
//    if (response.StatusCode == HttpStatusCode.OK)
//    {
//        Stream receiveStream = response.GetResponseStream();
//        StreamReader readStream = null;
//        if (response.CharacterSet == null)
//            readStream = new StreamReader(receiveStream);
//        else
//            readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
//        data = readStream.ReadToEnd();
//        response.Close();
//        readStream.Close();
//    }
//    //MatchCollection collection = Regex.Matches(data, pattern);

//    MatchCollection collection = rr.Matches(data);

//    StringBuilder builder = new StringBuilder();
//    foreach (var item in collection)
//        builder.Append(item.ToString() + "\n");

//    MessageBox.Show(builder.ToString() + "\n");
//}



//const string pattern = @"<(a|link).*?href=(""|')(.+?)(""|').*?>";
//private Regex rr = new Regex("<a (.*\\s)?href=\"?'?(.*?)\"?'?(\\s.*)?>");