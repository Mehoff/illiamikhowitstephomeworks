﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class ClientForm : Form
    {
        private static bool isRunning;
        private static Thread thread;
        private static IPAddress remoteIPAddr;
        private static int port;
        public ClientForm()
        {
            InitializeComponent();
            DefaultInitialization();

            thread = new Thread(new ThreadStart(DoWork));
        }
        private void DefaultInitialization() 
        {
            textBoxIp.Text = "127.0.0.1";
            textBoxPort.Text = "3000";
            labelTime.Text = "00:00:00";
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            remoteIPAddr = IPAddress.Parse(textBoxIp.Text);
            port = Convert.ToInt32(textBoxPort.Text);
            isRunning = true;
            thread.Start();
        }
        private void DoWork() 
        {
            try
            {
                while (isRunning) 
                {
                    var udpClient = new UdpClient(port);

                    IPEndPoint ipEnd = null;
                    byte[] response = udpClient.Receive(ref ipEnd);

                    string strResult = Encoding.Unicode.GetString(response);

                    labelTime.BeginInvoke((MethodInvoker)(() =>
                        labelTime.Text = $"Время: {strResult}"
                    ));

                    udpClient.Close();
                }
            }
            catch (SocketException sockEx)
            {
                MessageBox.Show($"Ошибка сокета: {sockEx.Message}");
            }
            catch (Exception ex) 
            {
                MessageBox.Show($"Ошибка: {ex.Message}");
            }
        }

        private void ClientForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            isRunning = false;
            thread.Abort();
        }
    }
}
