﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection.Metadata.Ecma335;

namespace CalculatorNamespace
{
    public class Fraction
    {
        public int numerator, pointer;

        public Fraction(int n, int p) 
        {
            numerator = n;
            pointer = p;
            FixNegativePointer();
        }
        private void FixNegativePointer() 
        {
            if (this.pointer < 0 || this.pointer < 0 && this.numerator < 0)
            {
                this.numerator *= -1;
                this.pointer *= -1;
            }
        }
        public static void MakeSamePointer(Fraction f1, Fraction f2) 
        {
            if (f1.pointer == f2.pointer)
                return;

            var cpy = new Fraction(f1.numerator, f1.pointer);

            f1.MultiplyByFraction(f2.pointer, f2.pointer);
            f2.MultiplyByFraction(cpy.pointer, cpy.pointer);
        }
        public void AddFraction(Fraction other) 
        {
            if (other.pointer != pointer)
            {
                MakeSamePointer(this, other);
            }
            this.numerator += other.numerator;
            FixNegativePointer();
        }
        public void MultiplyByFraction(Fraction other) 
        {
            numerator *= other.numerator;
            pointer *= other.pointer;
            FixNegativePointer();

        }
        public void MultiplyByFraction(int _numerator, int _pointer)
        {
            numerator *= _numerator;
            pointer *= _pointer;
            FixNegativePointer();
        }
        public void MultiplyByValue(int value) 
        {
            numerator *= value;
            FixNegativePointer();
        }
        public void SubtractFraction(Fraction other) 
        {
            if (this.pointer != other.pointer)
            {
                MakeSamePointer(this, other);
            }
            this.numerator -= other.numerator;
            FixNegativePointer();
        }
        public void DivideByFraction(Fraction other) 
        {
            if (other.numerator == 0 || other.pointer == 0)
                throw new DivideByZeroException();

            this.numerator *= other.pointer;
            this.pointer *= other.numerator;
            FixNegativePointer();
        }
        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            if (object.ReferenceEquals(this, other))
                return true;
            if (this.GetType() != other.GetType())
                return false;
            return Equals(other as Fraction);
        }
        public bool Equals(Fraction other) 
        {
            if (other == null)
                return false;
            if (object.ReferenceEquals(this, other))
                return true;
            if (this.GetType() != other.GetType())
                return false;

            if (this.pointer != other.pointer)
            {
                var t_cpy = new Fraction(this.numerator, this.pointer);
                var o_cpy = new Fraction(other.numerator, other.pointer);

                MakeSamePointer(t_cpy, o_cpy);

                if (t_cpy.numerator == o_cpy.numerator)
                    return true;
                else return false;
            }
            else 
            {
                if (this.numerator == other.numerator)
                    return true;
                else return false;
            }
        }
        public override string ToString()
        {
            return $"{numerator}/{pointer}";
        }
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            Fraction f1 = new Fraction(-4, 5);

            Fraction f2 = new Fraction(-2, 8);

            f1.AddFraction(f2);

            Console.WriteLine(f1);

        }
    }
}
