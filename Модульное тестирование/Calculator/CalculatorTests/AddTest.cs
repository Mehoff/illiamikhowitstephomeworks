using NUnit.Framework;
using CalculatorNamespace;
using System;
using Newtonsoft.Json.Bson;

namespace CalculatorTests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        [TestCase(4, 5, 2, 8, 42, 40)]
        [TestCase(-4, 5, -2, 8, -42, 40)]
        [TestCase(4, -5, 2, -8, -42, 40)]
        [TestCase(-4, -5, -2, -8, 42, 40)]
        [TestCase(-4, 5, 2, 8, -22, 40)]


        public void AddFractionTest(int f1_n, int f1_p, 
                                    int f2_n, int f2_p,
                                    int r_n,  int r_p) 
        {
            var actual = new Fraction(f1_n, f1_p);
            var f2 = new Fraction(f2_n, f2_p);

            actual.AddFraction(f2);
            var expected = new Fraction(r_n, r_p);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(4, 5, 2, 8, 32, 10)]
        [TestCase(1, 12, 10, 5, 5, 120)]
        [TestCase(8, -5, -10, 2, 16, 50)]
        [TestCase(-8, -5, -10, -2, 16, 50)]

        public void DivideByFractionTest(int f1_n, int f1_p,
                                         int f2_n, int f2_p,
                                         int r_n, int r_p)
        {
            var actual = new Fraction(f1_n, f1_p);
            var f2 = new Fraction(f2_n, f2_p);

            actual.DivideByFraction(f2);
            var expected = new Fraction(r_n, r_p);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(2, 10, 0, 0)]
        [TestCase(2, 10, 0, 5)]
        [TestCase(2, 10, 5, 0)]
        public void DivideByZeroTest(int f1_n, int f1_p, int f2_n, int f2_p)
        {
            var f1 = new Fraction(f1_n, f1_p);
            var f2 = new Fraction(f2_p, f2_n);

            Assert.Throws<DivideByZeroException>(() => f1.DivideByFraction(f2));
        }
        [Test]
        [TestCase(-2, 5, 4, 8, -8, 40)]
        [TestCase(-2, 5, -4, 8, 8, 40)]
        [TestCase(2, -5, 4, -8, 8, 40)]
        [TestCase(2, -5, -4, 8, 8, 40)]
        public void MultiplyByFractionTest(int f1_n, int f1_p,
                                           int f2_n, int f2_p,
                                           int r_n, int r_p)
        {
            var actual = new Fraction(f1_n, f1_p);
            var f2 = new Fraction(f2_n, f2_p);

            actual.MultiplyByFraction(f2);

            var expected = new Fraction(r_n, r_p);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        [TestCase(5, 10, 4, 10, 1, 10)]
        [TestCase(4, 5, 2, 3, 2, 15)]
        [TestCase(-4, 5, -2, 3, -2, 15)]
        [TestCase(4, -5, 2, -3, -2, 15)]
        [TestCase(4, 5, 2, -3, 22, 15)]
        public void SubtractFractionTest(int f1_n, int f1_p,
                                         int f2_n, int f2_p,
                                         int r_n, int r_p)
        {
            var actual = new Fraction(f1_n, f1_p);
            var f2 = new Fraction(f2_n, f2_p);

            actual.SubtractFraction(f2);

            var expected = new Fraction(r_n, r_p);
            Assert.AreEqual(expected, actual);
        }

    }
}