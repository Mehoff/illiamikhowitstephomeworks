﻿namespace TestsWPF.Classes
{
    public static class TestsLoader
    {
        // Settings

        const int TEST_COUNT = 3;
        const int QUESTION_COUNT = 3;
        const int ANSWER_COUNT = 4;

        public static Test[] LoadTests()
        {
            Test[] tests = new Test[TEST_COUNT];
            Question[] questions = new Question[QUESTION_COUNT];
            Answer[] answers = new Answer[ANSWER_COUNT];

            for (int i = 0; i < TEST_COUNT; i++) 
            {
                tests[i] = new Test();
                tests[i].Name = $"Тест {i}";

                for (int j = 0; j < QUESTION_COUNT; j++) 
                {
                    questions[j] = new Question();
                    questions[j].Text = $"Вопрос {j}";

                    for (int z = 0; z < ANSWER_COUNT; z++) 
                    {
                        answers[z] = new Answer();

                            if (z == 1)
                                answers[z].IsRight = true;

                        answers[z].Text = $"Ответ {z + 1}";
                    }
                    questions[j].Answers = answers;
                }
                tests[i].Questions = questions;
            }
            return tests;
        }

        public static Test GetTestWithCheckBoxes() 
        {
            Test test = new Test();
            Question[] questions = new Question[3];
            Answer[] answers = new Answer[4];

            test.Name = "С чекбоксами";

            for (int i = 0; i < 3; i++) 
            {
                questions[i] = new Question();
                questions[i].Text = $"Вопрос {i + 1}";
                for (int j = 0; j < 4; j++) 
                {
                    answers[j] = new Answer();

                    if (j != 3)
                        answers[j].IsRight = true;
                    else answers[j].IsRight = false;

                    answers[j].Text = $"Ответ {j + 1}";

                }
                questions[i].Answers = answers;
            }
            test.Questions = questions;
            return test;
        }
    }
}
