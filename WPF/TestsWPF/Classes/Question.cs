﻿namespace TestsWPF.Classes
{
    public class Question
    {
        public Answer[] Answers { get; set; }
        public string Text { get; set; }
    }
}
