﻿namespace TestsWPF.Classes
{
    public class Answer
    {
        public bool IsRight { get; set; }
        public string Text { get; set; }
    }
}
