﻿using TestsWPF.Classes;

namespace TestsWPF
{
    public class Test
    {
        public Question[] Questions { get; set; }
        public string Name { get; set; }
    }
}
