﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TestsWPF.Classes;

namespace TestsWPF
{
    public enum RadioButtonOrCheckBox 
    {
        RadioButton,
        CheckBox
    };
    public partial class MainWindow : Window
    {
        public ObservableCollection<Test> testsCollection = new ObservableCollection<Test>();
        public Test currentTest = new Test();
        public List<Answer> currentQuestionUserAnswersList = new List<Answer>();
        public List<Answer> chosen_answers = new List<Answer>();
        public int currentQuestionIndex;

        public MainWindow()
        {
            InitializeComponent();

        }

        private void LoadTests() 
        {
            foreach (Test test in TestsLoader.LoadTests())
                testsCollection.Add(test);

            testsCollection.Add(TestsLoader.GetTestWithCheckBoxes());

            foreach (var test in testsCollection) 
            {
                RadioButton button = new RadioButton();
                button.Content = test.Name;
                button.Tag = test;

                button.Click += RadioButtonClick;


                TestsToolBar.Items.Add(button);
            }

        }

        private void RadioButtonClick(object sender, RoutedEventArgs e)
        {
            QuestionsTabControl.Items.Clear();

            var button = (RadioButton)sender;
            var test = (Test)button.Tag;

            currentTest = test;
            currentQuestionIndex = 0;


            for (int i = 0; i < currentTest.Questions.Length; i++) 
            {
                TabItem item = new TabItem();
                item.Header = i;

                SetTabItemContent(item, currentTest.Questions[i]);

                item.IsEnabled = false;
                QuestionsTabControl.Items.Add(item);
            }
        }

        private void SetTabItemContent(TabItem item, Question question)
        {
            StackPanel stackPanel = new StackPanel() { Margin = new Thickness(10, 5, 0, 0) };

            RadioButtonOrCheckBox flag = RadioButtonOrCheckBox.RadioButton;
            if (question.Answers.Count(q => q.IsRight == true) > 1)
                flag = RadioButtonOrCheckBox.CheckBox;

            stackPanel.Children.Add(new TextBlock() { Text = $"{question.Text}", Margin = new Thickness(0, 10, 0, 5) });

            int i = 0;

            for ( ; i < question.Answers.Length; i++) 
            {
                var answer = question.Answers[i];

                switch (flag)
                {
                    case RadioButtonOrCheckBox.CheckBox:
                        CheckBox checkBox = new CheckBox()
                        {
                            Content = $"{answer.Text}",
                            Tag = new object[] { typeof(CheckBox), answer },
                            Margin = new Thickness(0, 10, 0, 0)
                        };

                        checkBox.Click += Answer_Checked;
                        stackPanel.Children.Add(checkBox);
                        break;
                    case RadioButtonOrCheckBox.RadioButton:
                        RadioButton radioButton = new RadioButton()
                        {
                            Content = $"{answer.Text}",
                            Tag = new object[] { typeof(RadioButton), answer },
                            Margin = new Thickness(0, 10, 0, 0)
                        };

                        radioButton.Checked += Answer_Checked;
                        stackPanel.Children.Add(radioButton);
                        break;
                    default: throw new Exception("Exception");
                }
            }

            Button button = new Button() { Content = "N E X T", Margin = new Thickness(0, 10, 0, 0), Tag = i };
            button.Click += Button_Click;
            stackPanel.Children.Add(button);
            item.Content = stackPanel;
        }

        private void Answer_Checked(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;

            object[] objects = (object[])control.Tag;

            Type type = (Type)objects[0];

            Answer answer = (Answer)objects[1];

            switch (type.Name.ToLower())
            {
                case "radiobutton":
                    RadioButton radioButton = (RadioButton)sender;

                    currentQuestionUserAnswersList.Clear();

                    if(radioButton.IsChecked == true) 
                    {
                        currentQuestionUserAnswersList.Add(answer);
                    }
                    else { currentQuestionUserAnswersList.Remove(answer); }
                    break;
                case "checkbox":
                    CheckBox checkBox = (CheckBox)sender;

                    if (checkBox.IsChecked == true)
                    {
                        currentQuestionUserAnswersList.Add(answer);
                    }
                    else
                    {
                        currentQuestionUserAnswersList.Remove(answer);
                    }
                    break;
                default: throw new Exception("Exception");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            chosen_answers.AddRange(currentQuestionUserAnswersList.ToArray());

            currentQuestionUserAnswersList.Clear();

            QuestionsTabControl.SelectedIndex++;
            currentQuestionIndex++;

            int right = 0;
            int all = 0;

            if(currentQuestionIndex == currentTest.Questions.Length) 
            {
                foreach (var question in currentTest.Questions) 
                {
                    all += question.Answers.Count(a => a.IsRight == true);
                }

                right += chosen_answers.Count(a => a.IsRight == true);

                MessageBox.Show($"Максимальное количество баллов: {all}\nВы набрали: {right}");

                chosen_answers.Clear();
                QuestionsTabControl.Items.Clear();
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadTests();
        }
    }
}
