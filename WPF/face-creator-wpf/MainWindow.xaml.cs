﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FaceCreator
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadPartsToComboxes();

            SetDefaultFace();

            LoadMasks();
        }
        private void SetDefaultFace() 
        {
            try
            {
                comboBoxEars.SelectedIndex = 0;
                comboBoxEyes.SelectedIndex = 0;
                comboBoxHairstyles.SelectedIndex = 0;
                comboBoxMouthes.SelectedIndex = 0;
                comboBoxNoses.SelectedIndex = 0;
            }
            catch 
            {
                throw new Exception("Не хватает картинок в каком-то из ComboBox`ов");
            }
        }

        private void LoadPartsToComboxes()
        {
            LoadEars();
            LoadEyes();
            LoadHarstyles();
            LoadMouthes();
            LoadNoses();
        }

        private void ListBoxMasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lBox = (ListBox)sender;

            var selected = (ListBoxItem)lBox.SelectedItem;

            TextReader reader = new StreamReader(selected.Tag.ToString());

            comboBoxEars.SelectedIndex = Convert.ToInt32(reader.ReadLine());
            comboBoxEyes.SelectedIndex = Convert.ToInt32(reader.ReadLine());
            comboBoxHairstyles.SelectedIndex = Convert.ToInt32(reader.ReadLine());
            comboBoxMouthes.SelectedIndex = Convert.ToInt32(reader.ReadLine());
            comboBoxNoses.SelectedIndex = Convert.ToInt32(reader.ReadLine());

            reader.Close();
        }

        private void LoadMasks() 
        {
            ListBoxMasks.Items.Clear();


            if (!Directory.Exists("..\\..\\..\\Masks"))
            {
                Directory.CreateDirectory("..\\..\\..\\Masks");
            }

            string[] paths = Directory.GetFiles("..\\..\\..\\Masks", "*.txt");

            foreach(var path in paths)
            {
                var name = System.IO.Path.GetFileNameWithoutExtension(path);
                ListBoxItem item = new ListBoxItem
                {
                    Content = name,
                    Tag = path
                };

                ListBoxMasks.Items.Add(item);
            }
        }
        
        private void LoadEars() 
        {
            string[] paths = Directory.GetFiles("../../../Resources/Ears", "*.png");
            foreach (var path in paths) 
            {
                int pFrom = path.IndexOf("\\") + "\\".Length;
                int pTo = path.LastIndexOf(".png");
                string name = path.Substring(pFrom, pTo - pFrom);

                ComboBoxItem item = new ComboBoxItem
                {
                    Content = name,
                    Tag = path
                };
                comboBoxEars.Items.Add(item);
            }
        }
        private void LoadEyes()
        {
            string[] paths = Directory.GetFiles("../../../Resources/Eyes", "*.png");
            foreach (var path in paths)
            {
                int pFrom = path.IndexOf("\\") + "\\".Length;
                int pTo = path.LastIndexOf(".png");
                string name = path.Substring(pFrom, pTo - pFrom);

                ComboBoxItem item = new ComboBoxItem
                {
                    Content = name,
                    Tag = path
                };

                comboBoxEyes.Items.Add(item);
            }
        }

        private void LoadHarstyles()
        {
            string[] paths = Directory.GetFiles("../../../Resources/Hairstyles", "*.png");
            foreach (var path in paths)
            {
                int pFrom = path.IndexOf("\\") + "\\".Length;
                int pTo = path.LastIndexOf(".png");
                string name = path.Substring(pFrom, pTo - pFrom);

                ComboBoxItem item = new ComboBoxItem
                {
                    Content = name,
                    Tag = path
                };

                comboBoxHairstyles.Items.Add(item);
            }
        }

        private void LoadMouthes()
        {
            string[] paths = Directory.GetFiles("../../../Resources/Mouthes", "*.png");
            foreach (var path in paths)
            {
                int pFrom = path.IndexOf("\\") + "\\".Length;
                int pTo = path.LastIndexOf(".png");
                string name = path.Substring(pFrom, pTo - pFrom);

                ComboBoxItem item = new ComboBoxItem
                {
                    Content = name,
                    Tag = path
                };

                comboBoxMouthes.Items.Add(item);
            }
        }
        private void LoadNoses()
        {
            string[] paths = Directory.GetFiles("../../../Resources/Noses", "*.png");
            foreach (var path in paths)
            {
                int pFrom = path.IndexOf("\\") + "\\".Length;
                int pTo = path.LastIndexOf(".png");
                string name = path.Substring(pFrom, pTo - pFrom);

                ComboBoxItem item = new ComboBoxItem
                {
                    Content = name,
                    Tag = path
                };

                comboBoxNoses.Items.Add(item);
            }
        }


        private void Ears_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ComboBoxItem)e.AddedItems[0];
            
            LeftEar.Source = new BitmapImage(new Uri(item.Tag.ToString(), UriKind.Relative));
            RightEar.Source = new BitmapImage(new Uri(item.Tag.ToString(), UriKind.Relative));

        }

        private void Eyes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ComboBoxItem)e.AddedItems[0];

            Eyes.Source = new BitmapImage(new Uri(item.Tag.ToString(), UriKind.Relative));
        }

        private void Hairstyles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ComboBoxItem)e.AddedItems[0];

            Hair.Source = new BitmapImage(new Uri(item.Tag.ToString(), UriKind.Relative));
        }

        private void Mouthes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ComboBoxItem)e.AddedItems[0];

            Mouth.Source = new BitmapImage(new Uri(item.Tag.ToString(), UriKind.Relative));
        }

        private void Noses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ComboBoxItem)e.AddedItems[0];

            Nose.Source = new BitmapImage(new Uri(item.Tag.ToString(), UriKind.Relative));
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            TextWriter writer = new StreamWriter($"../../../Masks/{TextBoxTemplateName.Text}.txt");

            writer.WriteLine(comboBoxEars.SelectedIndex);
            writer.WriteLine(comboBoxEyes.SelectedIndex);
            writer.WriteLine(comboBoxHairstyles.SelectedIndex);
            writer.WriteLine(comboBoxMouthes.SelectedIndex);
            writer.WriteLine(comboBoxNoses.SelectedIndex);

            writer.Close();

            LoadMasks();
        }
    }
}
