﻿using CalendarWPF.Classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace CalendatWPF
{
    public enum DayColor {White = 1, Green, Blue, Red}
    public struct MarkedDay
    {
        public Month Month { get; set; }
        public int Number { get; set; }
        public DayColor Color { get; set; }
        public void ChangeColor(Button button) 
        {
            if (Color == DayColor.Red)
                Color = DayColor.White;

            else Color++;

            SetButtonColor(button);
        }
        private void SetButtonColor(Button button) 
        {
            switch (Color) 
            {
                case DayColor.White: button.Background = new SolidColorBrush(Colors.WhiteSmoke); break;
                case DayColor.Green: button.Background = new SolidColorBrush(Colors.Green); break;
                case DayColor.Blue:  button.Background = new SolidColorBrush(Colors.Blue); break;
                case DayColor.Red:   button.Background = new SolidColorBrush(Colors.Red); break;
            }
        }
    };
    public partial class MainWindow : Window
    {
        public Year Year { get; set; }
        public Month CurrentMonth { get; set; }
        public ObservableCollection<Week> Weeks { get; set; }

        public List<MarkedDay> MarkedDays = new List<MarkedDay>();

        public MainWindow()
        {
            InitializeComponent();   
        }

        private void MonthRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton button = (RadioButton)sender;

            LoadMonth((MonthName)Convert.ToInt32(button.Tag));

            LoadWeeks(CurrentMonth);

            lsView.ItemsSource = Weeks;
        }
        private void DayClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;

            if (button.Content.ToString() == string.Empty)
                return;

            int number = Convert.ToInt32(button.Content.ToString());

            bool foundFlag = false;

            foreach (var item in MarkedDays)
            {
                if (item.Number == number && item.Month == CurrentMonth)
                {
                    foundFlag = true;
                    item.ChangeColor(button);
                    break;
                }
            }

            if (!foundFlag) 
            {
                MarkedDay newDay = new MarkedDay { Month = CurrentMonth, Number = number, Color = DayColor.White };
                newDay.ChangeColor(button);
                MarkedDays.Add(newDay);
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Year = new Year();
        }

        private void LoadWeeks(Month month) 
        {
            Weeks = new ObservableCollection<Week>();

            var week = new Week();
            for (int i = 0; i < month.Days.Count; i++) 
            {
                // Их видно
                if (MarkedDays.Count(day => day.Month == CurrentMonth && day.Number == i) > 0) 
                {
                    MessageBox.Show($"{CurrentMonth.Name} - {i}");
                }

                switch (month.Days[i].DayOfWeek) 
                {
                    case CalendarWPF.Classes.DayOfWeek.Monday:
                        week = new Week();
                        week.Monday = month.Days[i].Number.ToString();
                        break;
                    case CalendarWPF.Classes.DayOfWeek.Tuesday:
                        week.Tuesday = month.Days[i].Number.ToString();
                        break;
                    case CalendarWPF.Classes.DayOfWeek.Wednesday:
                        week.Wednesday = month.Days[i].Number.ToString();
                        break;
                    case CalendarWPF.Classes.DayOfWeek.Thursday:
                        week.Thursday = month.Days[i].Number.ToString();
                        break;
                    case CalendarWPF.Classes.DayOfWeek.Friday:
                        week.Friday = month.Days[i].Number.ToString();
                        break;
                    case CalendarWPF.Classes.DayOfWeek.Saturday:
                        week.Saturday = month.Days[i].Number.ToString();
                        break;
                    case CalendarWPF.Classes.DayOfWeek.Sunday:
                        week.Sunday = month.Days[i].Number.ToString();
                        Weeks.Add(week);
                        week = new Week();
                        break;
                }
                if (i == month.Days.Count - 1 
                    && month.Days[i].DayOfWeek 
                    != CalendarWPF.Classes.DayOfWeek.Sunday)
                {
                    Weeks.Add(week);
                    week = new Week();
                }
            }
        }

        private void LoadMonth(MonthName month) 
        {
            CurrentMonth = Year.Months[(int)month];
        }
    }
}
