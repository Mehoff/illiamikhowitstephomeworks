﻿#include <iostream>

class ArrowClock
{
private:
    int hourArrow;    // 1,2,3 . . . 12
    int minutesArrow; // 0,1,2 . . . 59, 0
    bool meridiem;    // true = AM / false = PM
public:

    ArrowClock(int h, int m, bool _meridiem)
    {
        if (h > 12 || m > 59 || h <= 0 || m < 0) 
        {
            std::cout << "Wrong parameters, setting default values\n";

            hourArrow = 1;
            minutesArrow = 0;
            meridiem = true;
        }
        else
        {
            hourArrow = h;
            minutesArrow = m;
            meridiem = _meridiem;
        }
    }
    void showTime() 
    {
        std::cout << "Arrow Clock Says:\n" << hourArrow << ":" << minutesArrow << meridiem << "\n";
    }

    //getters

    int& getHourArrowValue() { return hourArrow; }
    int& getMinuteArrowValue() { return minutesArrow; }
    bool& getMeridiem() { return meridiem; }
};


class DigitalClock 
{
public:
    virtual ~DigitalClock() {}
    virtual void showTime() = 0;
};

class DigitalClockAdapter : public DigitalClock
{
private:
    ArrowClock * arrowclock;
public:
    DigitalClockAdapter(ArrowClock* clock) : arrowclock(clock) { }
    ~DigitalClockAdapter() override { delete arrowclock; }

    void showTime() override 
    {
        std::cout << "Digital Clock Says:\n";

        int a_hours   = arrowclock->getHourArrowValue();
        int a_minutes = arrowclock->getMinuteArrowValue();
        bool meridiem = arrowclock->getMeridiem();

        int d_hours;

        meridiem ? d_hours = a_hours : d_hours = a_hours + 12;
        
        std::cout << d_hours << ":" << a_minutes << "\n";

    }
};


int main()
{

    DigitalClock* clock = new DigitalClockAdapter(new ArrowClock(8, 20, false));

    clock->showTime();
}

