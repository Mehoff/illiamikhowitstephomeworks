﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.InteropServices;

namespace Observer_gai_
{

    public interface ITrafficPoliceDepartment 
    {
        void Update(PoliceNotifier policeNotifier, string message);
    }

    public class PoliceNotifier  
    {
        public string Name { get; set; }
        private readonly List<ITrafficPoliceDepartment> departments;

        public PoliceNotifier(string name) 
        {
            Name = name;
            departments = new List<ITrafficPoliceDepartment>();
        }
        public void AddDepartment(ITrafficPoliceDepartment department) 
        {
            departments.Add(department);
        }
        public void RemoveDepartment(ITrafficPoliceDepartment department)
        {
            departments.Remove(department);
        }

        public void Notify(string message) 
        {
            foreach (var department in departments) 
            {
                department.Update(this, message);
            }
        }
    }

    public class TrafficPoliceDepartment : ITrafficPoliceDepartment 
    {
        string Name { get; set; }
        public TrafficPoliceDepartment(string name) { Name = name; }

        void ITrafficPoliceDepartment.Update(PoliceNotifier policeNotifier, string message)
        {
            Console.WriteLine($"{policeNotifier.Name} вызывает {Name}.\n Сообщение: {message}");
        }
    } 
    class Program
    {
        static void Main(string[] args)
        {
            TrafficPoliceDepartment dep1 = new TrafficPoliceDepartment("Отделение Трасса E95");
            TrafficPoliceDepartment dep2 = new TrafficPoliceDepartment("Отделение Трасса Одесса-Рени");
            TrafficPoliceDepartment dep3 = new TrafficPoliceDepartment("Отделение Центр Киев");
            PoliceNotifier notifier = new PoliceNotifier("Главное Киевское ГАИ");

            notifier.AddDepartment(dep1);
            notifier.AddDepartment(dep2);
            notifier.AddDepartment(dep3);

            notifier.Notify("Вызванные посты, проверяйте все автомобили модели ГАЗ 2105 чёрного цвета");
           
        }
    }
}
