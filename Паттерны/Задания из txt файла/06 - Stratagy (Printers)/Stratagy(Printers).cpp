#include <iostream>

class IPrint 
{
public:
    ~IPrint() {}
    virtual void print() = 0;
};

class CD_Print : public IPrint
{
public:
    void print() override 
    {
        std::cout << "CD Printer Print\n";
    }
};

class BlackWhite_Print : public IPrint
{
public:
    void print() override
    {
        std::cout << "Black&White Printer Print\n";
    }
};

class ColourfulPrint : public IPrint
{
public:
    void print() override
    {
        std::cout << "Colourful Printer Print\n";
    }
};

class Printer 
{
private:
    IPrint* p;
public:
    Printer(IPrint* _p) : p(_p) {}
    ~Printer() { delete p; }

    void print() 
    {
        p->print();
    }
};

int main()
{
    Printer* printer = new Printer(new ColourfulPrint);
    printer->print();

    delete printer;
}
