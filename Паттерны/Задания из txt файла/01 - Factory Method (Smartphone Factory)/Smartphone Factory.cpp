﻿#include <iostream>
#include <vector>

enum BrandName 
{
SamsungPhone,
NokiaPhone,
HuaweiPhone,
XiaomiPhone,
ApplePhone
};

struct Resolution 
{
private:
    int width;
    int height;

public:
    
    Resolution() { width = 0; height = 0; }
    Resolution(int w, int h) { width = w; height = h; }
    Resolution(const Resolution& res) { this->height = res.height; this->width = res.width; }

    void setResulution(int h, int w)
    {
        width = w;
        height = h;
    }
    void Show() 
    {
        std::cout << "Resolution: " << height << "x" << width << "\n";
    }
};

struct Battery 
{
private:
    int capacity;
    std::string brand;
public:

    Battery() { brand = "undefined"; capacity = -1; }
    Battery(std::string _brand, int _capacity) { brand = _brand; capacity = _capacity; }
    Battery(const Battery& bat) { this->brand = bat.brand; this->capacity = bat.capacity; }

    void setSpecs(std::string _brand, int _capacity) { brand = _brand; capacity = _capacity; }
    void Show() { std::cout << "Battery:\n" << "Brand: " << brand << " | Capacity: " << capacity << " MA\n"; }

};

class Smartphone 
{
protected:
    std::string model;
    Resolution resolution;
    Battery battery;
    int storage;

public:

    Smartphone() 
    {
        model = "undefined";
        resolution.setResulution(-1, -1);
        battery.setSpecs("undefined", -1);
        storage = -1;
    }

    virtual void Show() 
    {
        std::cout << "Model: " << model << "\n";
        resolution.Show();
        battery.Show();
        std::cout << "Storage: " << storage << " gb\n";
        std::cout << "- / - / - / - / - / - / - / - / - / -\n";
    }

    static Smartphone* createSmartphone(BrandName name);
};

class Samsung : public Smartphone
{
public:
    Samsung() 
    {
        
        model = "Samsung Galaxy S20";
        resolution.setResulution(3200, 1400);
        battery.setSpecs("Samsung", 4000);
        storage = 128;
    }
    void Show() override
    {
        Smartphone::Show();
    }
};

class Nokia : public Smartphone
{
public:
    Nokia()
    {
        model = "Nokia X20";
        resolution.setResulution(2560, 1440);
        battery.setSpecs("Nokia", 3000);
        storage = 64;
    }
    void Show() override
    {
        Smartphone::Show();
    }
};

class Huawei : public Smartphone
{
public:
    Huawei()
    {
        model = "Huawei P20 Pro";
        resolution.setResulution(2560, 1440);
        battery.setSpecs("Huawei", 5000);
        storage = 256;
    }
    void Show() override
    {
        Smartphone::Show();
    }
};

class Xiaomi : public Smartphone
{
public:
    Xiaomi()
    {
        model = "Xiaomi Mi A3";
        resolution.setResulution(1560, 720);
        battery.setSpecs("Li-Po", 4030);
        storage = 128;
    }
    void Show() override
    {
        Smartphone::Show();
    }
};

class Apple : public Smartphone 
{
public:
    Apple()
    {
        model = "Apple IPhone X";
        resolution.setResulution(2436, 1125);
        battery.setSpecs("Li-Po", 2716);
        storage = 256;
    }
    void Show() override
    {
        Smartphone::Show();
    }
};

Smartphone* Smartphone::createSmartphone(BrandName name) 
{
    Smartphone * s_ptr;

    switch (name)
    {
    case SamsungPhone: 
        s_ptr = new Samsung();
        break;
    case NokiaPhone:
        s_ptr = new Nokia();
        break;
    case HuaweiPhone:
        s_ptr = new Huawei();
        break;
    case XiaomiPhone:
        s_ptr = new Xiaomi();
        break;
    case ApplePhone:
        s_ptr = new Apple();
        break;
    default:
        s_ptr = new Smartphone();
        break;
    }

    return s_ptr;
}


int main()
{
    std::vector<Smartphone*> phones;

    phones.push_back(Smartphone::createSmartphone(SamsungPhone));
    phones.push_back(Smartphone::createSmartphone(NokiaPhone));
    phones.push_back(Smartphone::createSmartphone(XiaomiPhone));
    phones.push_back(Smartphone::createSmartphone(HuaweiPhone));
    phones.push_back(Smartphone::createSmartphone(ApplePhone));

    for (auto& o : phones) { o->Show(); }
}

