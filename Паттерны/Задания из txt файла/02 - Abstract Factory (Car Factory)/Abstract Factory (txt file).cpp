﻿#include <iostream>

class IEngine 
{
public:
	virtual void Start() = 0;
};

class ICar 
{
private:
	IEngine* engine = nullptr;
public:
	virtual void Start() = 0;
	void setEngine(IEngine* en) { engine = en; }
	IEngine *& getEngine() { return engine; }
};

class Toyota : public ICar 
{
	void Start() override 
	{
		if (getEngine() != nullptr) 
		{
			getEngine()->Start();
		}
		else { std::cout << "Toyota has no engine inside\n"; }
	}
};

class ToyotaEngine : public IEngine
{
	void Start()
	{
		std::cout << "Starting up Toyota Engine\n";
	}
};

class Nissan : public ICar 
{
public:
	void Start() override 
	{
		if (getEngine() != nullptr)
		{
			getEngine()->Start();
		}
		else { std::cout << "Nissan has no engine inside\n"; }
	}
};

class NissanEngine : public IEngine 
{
	void Start()
	{
		std::cout << "Starting up Nissan Engine\n";
	}
};

class ICarFactory 
{
public:
	virtual ICar * createCar() = 0;
};

class ToyotaCarFactory : public ICarFactory
{
public:
	ICar* createCar() override
	{
		ICar* newCar = new Toyota();
		IEngine * newEngine = new ToyotaEngine();
		newCar->setEngine(newEngine);

		return newCar;
	}
};

class NissanCarFactory : public ICarFactory
{
public:
	ICar* createCar() override 
	{
		ICar* newCar = new Nissan();
		IEngine* newEngine = new NissanEngine();
		newCar->setEngine(newEngine);

		return newCar;
	}
};


int main()
{
	ToyotaCarFactory t_factory;
	NissanCarFactory n_factory;

	auto car = t_factory.createCar();

	car->Start();

	car = n_factory.createCar();
	car->Start();
}

