#include <iostream>
#include <stdexcept>
#include <vector>

struct CommandStruct 
{
public:
	char _operator;
	double _operand;

	CommandStruct(char c, double d) : _operator(c), _operand(d) {}
};

class ICommand
{
public:
	virtual void Execute() = 0;
	virtual void Undo() = 0;
};

class CalculatorCommand;

class Calculator 
{
private:
	std::vector<CommandStruct> commands;
	double current;
	double temp;
public:
	Calculator() 
	{
		current = 0;
		temp = 0;
	}

	void Action(char __operator, double __operand) 
	{
		temp = current;

		switch (__operator)
		{
		case '+': current += __operand;
			break;
		case '-': current -= __operand;
			break;
		case '/': current /= __operand;
			break;
		case '*': current *= __operand;
			break;
		default: throw new std::exception("Calculator::Action() invalid operator exception");
		}
		std::cout << "Current: " << temp << std::endl;
		std::cout << "- - - - -" << std::endl;
		std::cout << temp << " " << __operator << " " << __operand << " = " << current << "\n\n";

		commands.push_back(CommandStruct(__operator, __operand));
	}

	void Redo() 
	{
		if (!commands.empty())
		{
			Action(commands[commands.size() - 1]._operator, commands[commands.size() - 1]._operand);
		}
	}

	void Redo(CalculatorCommand* command);
	
};


class CalculatorCommand : public ICommand
{
private:
	Calculator* reciever;
	char _operator;
	double _operand;

public:
	CalculatorCommand(Calculator* _reciever, char __operator, double __operand)
	{
		reciever  = _reciever;
		_operator = __operator;
		_operand  = __operand;
	}
	void Execute() override
	{
		reciever->Action(_operator, _operand);
	}
	void Undo() override
	{
		reciever->Action(Undo(_operator), _operand);
	}

	char   & getOperator() { return _operator; }
	double & getOperand() { return _operand; }

private:

	char Undo(char op)
	{
		switch (op)
		{
		case '+': return '-';
			break;
		case '-': return '+';
			break;
		case '*': return '/';
			break;
		case '/': return '*';
			break;
		default: throw new std::exception("CalculatorCommand:Undo(char op) - invalid operator");
		}
	}

};

void Calculator::Redo(CalculatorCommand* command)
{
	Action(command->getOperator(), command->getOperand());
}


int main()
{
	Calculator* calc = new Calculator();

	CalculatorCommand* command  = new CalculatorCommand(calc, '+', 20);
	CalculatorCommand* command2 = new CalculatorCommand(calc, '-', 20);
	CalculatorCommand* command3 = new CalculatorCommand(calc, '*', 2);

	command->Execute();
	command2->Execute();
	command2->Undo();
	command3->Execute();

	calc->Redo();

	delete calc;
	delete command;
	delete command2;
	delete command3;
}

