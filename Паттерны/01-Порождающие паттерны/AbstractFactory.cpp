﻿#include <iostream>



// Abstract Product A
class Herbivore 
{
private:
    std::string name;
    int weight;
    bool alive;
  
public:
    virtual void EatGrass() = 0;
    virtual void SetParameters() = 0;
 
    bool& isAlive() { return alive; }
    int& getWeight() { return weight; }
    std::string& getName() { return name; }

    void Show() 
    {
        std::cout << name << std::endl;
        std::cout << weight << "kg\n";
        alive == true ? std::cout << "Alive\n" : std::cout << "Dead\n"; 
    }
};

// Abstract Product B
class Carnivore 
{
private:
    std::string name;
    int power;
public:
    virtual void EatHerbivore(Herbivore* animal) = 0;
    virtual void SetParameters() = 0;

    int& getPower() { return power; }
    std::string& getName() { return name; }
};

//Concrete Product A1
class Wildebeest : public Herbivore 
{
public:
    Wildebeest(){ SetParameters();}
    void EatGrass() override { getWeight() += 10; }
    void SetParameters() override 
    {
        isAlive() = true;
        getWeight() = 30;
        getName() = "Wilebeest";
    }
};

//Concrete Product A2
class Bison : public Herbivore
{
public:
    Bison() { SetParameters(); }
    void EatGrass() override { getWeight() += 10; }
    void SetParameters() override
    {
        isAlive() = true;
        getWeight() = 100;
        getName() = "Bison";
    }
};

//Concrete Product B1
class Lion : public Carnivore 
{
public:

    Lion() {SetParameters();}

    void EatHerbivore(Herbivore* animal) override
    {
        if (animal->getWeight() <= this->getPower()) 
        {
            std::cout << this->getName() << " had eaten " << animal->getName() << "\n";
            this->getPower() += 10;
            animal->isAlive() = false;
        }
        else 
        {
            std::cout << this->getName() << " struggled, and left " << animal->getName() << "\n";
            this->getPower() -= 10;
        }
    }
    void SetParameters() override 
    {
        this->getName() = "Lion";
        this->getPower() = 120;
    }
};

//Concrete Product B2
class Wolf : public Carnivore
{
public:

    Wolf() { SetParameters(); }

    void EatHerbivore(Herbivore* animal) override
    {
        if (animal->getWeight() <= this->getPower())
        {
            std::cout << this->getName() << " had eaten " << animal->getName() << "\n";
            this->getPower() += 10;
            animal->isAlive() = false;
        }
        else
        {
            std::cout << this->getName() << " struggled, and left " << animal->getName() << "\n";
            this->getPower() -= 10;
        }
    }
    void SetParameters() override
    {
        this->getName() = "Wolf";
        this->getPower() = 75;
    }
};

class IContinent
{
public:

    virtual Herbivore* createHerbivore() = 0;
    virtual Carnivore* createCarnivore() = 0;
};

class Africa : public IContinent
{
public:

    Herbivore* createHerbivore() override
    {
        Herbivore* an = new Wildebeest();
        return an;
    }

    Carnivore* createCarnivore() override
    {
        Carnivore* an = new Lion();
        return an;
    }
};

class NorthAmerica : public IContinent
{
public:
    Herbivore* createHerbivore() override
    {
        Herbivore* an = new Bison();
        return an;
    }
    Carnivore* createCarnivore() override
    {
        Carnivore* an = new Wolf();
        return an;
    }
};

class AnimalWorld 
{
public:
    void MealHerbivores(IContinent * continent) 
    {
        Herbivore * herb = continent->createHerbivore();
        herb->EatGrass();
    }
    void MealCarnivores(IContinent * continent) 
    {
        Carnivore* carn = continent->createCarnivore();
        Herbivore* hern = continent->createHerbivore();

        carn->EatHerbivore(hern);
    }
};

int main()
{
    AnimalWorld world;
    Africa africa;
    NorthAmerica n_america;

    world.MealCarnivores(&africa);
    world.MealHerbivores(&africa);

    world.MealCarnivores(&n_america);
    world.MealHerbivores(&n_america);
}
