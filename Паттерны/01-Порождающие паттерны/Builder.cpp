﻿#include <iostream>
#include <string>

enum Body {Coupe, Sedan, StationWagon, Hatchback};

class Car 
{
public:
	std::string * name;
	Body * bodyType;
    int * liters;
    int * wheels;
    bool * transmission;

    void Show() 
    {
        std::cout << *name << std::endl;
        
        switch (*bodyType) 
        {
        case Coupe:std::cout << "Coupe\n";
            break;
        case Sedan:std::cout << "Sedan\n";
            break;
        case StationWagon:std::cout << "StationWagon\n";
            break;
        case Hatchback:std::cout << "Hatchback\n";
            break;
        default: std::cout << "Undefined\n";
            break;
        }

        std::cout << "Liters: " << *liters << std::endl;

        std::cout << "Wheels: " << *wheels << std::endl;
        
        *transmission == true ? std::cout << "Manual\n" : std::cout << "Auto";
    }
};

class Builder 
{
public:
    virtual std::string * getName() = 0;
    virtual Body * getBody() = 0;
    virtual int* getLiters() = 0;
    virtual int * getWheels() = 0;
    virtual bool * getTransmission() = 0;
};


class DaewooBuilder : public Builder
{
public:

    std::string * getName() override 
    {
        std::string* name = new std::string("Daewoo Lanos");
        return name;
    }
    Body* getBody() override 
    {
        Body* _body = new Body(Sedan);
        return _body;
    }
    int* getLiters() override
    {
        int* _liters = new int(98);
        return _liters;
    }
    int* getWheels() override
    {
        int* _wheels = new int(13);
        return _wheels;
    }
    bool* getTransmission() override 
    {
        bool* _transmission = new bool(true);
        return _transmission;
    }
};

class FordBuilder : public Builder
{
public:
    std::string* getName() override
    {
        std::string* name = new std::string("Ford Probe");
        return name;
    }
    Body* getBody() override
    {
        Body* _body = new Body(Coupe);
        return _body;
    }
    int* getLiters() override
    {
        int* _liters = new int(160);
        return _liters;
    }
    int* getWheels() override
    {
        int* _wheels = new int(14);
        return _wheels;
    }
    bool* getTransmission() override
    {
        bool* _transmission = new bool(false);
        return _transmission;
    }
};

class UAZBuilder : public Builder
{
public:
    std::string* getName() override
    {
        std::string* name = new std::string("UAZ Patriot");
        return name;
    }
    Body* getBody() override
    {
        Body* _body = new Body(StationWagon);
        return _body;
    }
    int* getLiters() override
    {
        int* _liters = new int(120);
        return _liters;
    }
    int* getWheels() override
    {
        int* _wheels = new int(16);
        return _wheels;
    }
    bool* getTransmission() override
    {
        bool* _transmission = new bool(true);
        return _transmission;
    }
};

class HyundaiBuilder : public Builder 
{
public:
    std::string* getName() override
    {
        std::string* name = new std::string("Hyundai Getz");
        return name;
    }
    Body* getBody() override
    {
        Body* _body = new Body(Hatchback);
        return _body;
    }
    int* getLiters() override
    {
        int* _liters = new int(66);
        return _liters;
    }
    int* getWheels() override
    {
        int* _wheels = new int(13);
        return _wheels;
    }
    bool* getTransmission() override
    {
        bool* _transmission = new bool(false);
        return _transmission;
    }
};

class Director
{
private:
    Builder* builder;
public:
    void SetBuilder(Builder* _builder) 
    {
        builder = _builder;
    }
    Car* getCar() 
    {
        Car * car = new Car();
        
        car->name = builder->getName();
        car->bodyType = builder->getBody();
        car->liters = builder->getLiters();
        car->wheels = builder->getWheels();
        car->transmission = builder->getTransmission();

        return car;
    }
};



int main() 
{
    Car* car;

    Director director;

    DaewooBuilder d_builder;

    HyundaiBuilder h_builder;
   
    director.SetBuilder(&d_builder);
    
    car = director.getCar();

    car->Show();

    std::cout << "- - - - - - - - - -\n";

    director.SetBuilder(&h_builder);

    car = director.getCar();

    car->Show();

    return 0;
}


