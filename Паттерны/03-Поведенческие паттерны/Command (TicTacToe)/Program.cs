﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CommandCSharp
{

    public enum EPlayer {PlayerOne = 1, PlayerTwo = 2};

    public class TicTacToe 
    {
        public List<int[,]> boardStates = new List<int[,]>();

        public int CurrentPlayer;
        public bool isRunning { get; private set; }

        // 0 - empty, 1 - Player1, 2 - Player2
        public int[,] Board { get; set; }
        public TicTacToe() 
        {
            Board = new int[3, 3]
        {
            {0,0,0 },
            {0,0,0 },
            {0,0,0 },
        };
            boardStates.Add((int[,])Board.Clone());

            isRunning = true;
            CurrentPlayer = 1;

            UpdateUI();
        }
        public bool Turn(int x, int y) 
        {
            if (x > 2 || x < 0 || y > 2 || y < 0) { return false; }

            if (Board[x, y] == 0) 
            {
                ICommand command = new TicTacToeCommand(x, y, this);
                command.Execute();

                boardStates.Add((int[,])Board.Clone());
                return true; 
            }
            else
            {
                UpdateUI();
                return false;
            }
        }
        public void Undo()
        {
            if (boardStates.Count >= 2)
            {
                Board = boardStates[boardStates.Count - 2];
                boardStates.RemoveAt(boardStates.Count - 1);
                UpdateUI();
            }
        }
        public void UpdateUI() 
        {
            Console.Clear();

            Console.WriteLine($"Ход игрока {CurrentPlayer}\n");
            Console.WriteLine("Y----->");
            for(int i = 0; i < 3; i++)
            { 
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(Board[i, j] + " ");
                }
                if (i == 0)     { Console.Write("| X"); }
                else if (i < 2) { Console.Write("|"); }
                else            { Console.WriteLine("V"); }

                Console.WriteLine();
            }
            CheckWin();
            Console.WriteLine("\n");
        }
        public void SwitchPlayer() 
        {
            CurrentPlayer = CurrentPlayer == 1 ? CurrentPlayer = 2 : CurrentPlayer = 1;
        }
        public void CheckWin() 
        {
            int val = 0;
            for (int i = 0; i < 3; i++) 
            {
                //Horizontal Checks
                if (Board[i, val] == Board[i, val + 1] && Board[i, val] == Board[i, val + 2] && Board[i, val] != 0) 
                { ShowWinner(Board[i, val]); }
                    
                //Vertical Checks
                if (Board[val, i] == Board[val+1, i] && Board[val, i] == Board[val+2,i] && Board[val, i] != 0)
                { ShowWinner(Board[i, val]); }
            }
            // -> 
            if (Board[0, 0] == Board[0 + 1, 0 + 1] && Board[0, 0] == Board[0 + 2, 0 + 2] && Board[0, 0] != 0)
            { ShowWinner(Board[0, 0]); }
            // <-
            if (Board[2, 2] == Board[1, 1] && Board[0, 0] == Board[0, 2] && Board[2, 2] != 0)
            { ShowWinner(Board[2, 2]); }

           
        }

        public void ShowWinner(int Player) 
        {
            Console.WriteLine($"\n\n<!> Player {Player} won!");

            isRunning = false;
        }
    }

    public abstract class ICommand 
    {
        public abstract void Execute();
        public abstract void Undo();
    }

    public class TicTacToeCommand : ICommand
    {
        private TicTacToe ticTacToe;
        private int x, y;
        public TicTacToeCommand(int _x, int _y, TicTacToe _ticTacToe) 
        {
            x = _x;
            y = _y;
            ticTacToe = _ticTacToe;
        }
        public override void Execute()
        {
            ticTacToe.Board[x, y] = ticTacToe.CurrentPlayer;
            ticTacToe.UpdateUI();
            ticTacToe.SwitchPlayer();
        }
        public override void Undo()
        {
            ticTacToe.Undo();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            TicTacToe ticTacToe = new TicTacToe();
            while (ticTacToe.isRunning) 
            {
                Console.WriteLine("ESC(Undo) | ENTER(Next)");
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Enter)
                {
                    int x, y;
                    do
                    {
                        Console.WriteLine("X(0-2) : ");
                        x = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Y(0-2) : ");
                        y = Convert.ToInt32(Console.ReadLine());

                    } while (!ticTacToe.Turn(x, y));
                }
                else if(key.Key == ConsoleKey.Escape) 
                {
                    ticTacToe.Undo();
                }

            }
            Console.ReadKey();
        }
    }
}
