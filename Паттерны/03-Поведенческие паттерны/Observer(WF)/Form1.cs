﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Observer_WF_
{
    public partial class MainForm : Form
    {
        const int BUTTONS_COUNT = 5;
        const int LEFT_SPACING = 50;
        const int TOP = 10;

        public static List<MyButton> controls = new List<MyButton>();

        public interface IObserver 
        {
            void Update(ISubscriber subscriber, int x, int y);
        }
        public interface ISubscriber
        {
            void Add(IObserver observer);
            void Remove(IObserver observer);
        }
        public class MovableButton : Button, IObserver 
        {
            private bool isMoving;
            public MovableButton(int value) 
            {
                isMoving = false;
                Text = "OFF";

                Size = new Size(50, 30);
                this.Left = LEFT_SPACING * value;
                this.Top = TOP;
                this.Name = $"MovableButton{value + 1}";
                this.Location = new Point(LEFT_SPACING * value, TOP);

            }
            public void Update(ISubscriber subscriber, int x, int y) 
            {
                Point newPoint = new Point(this.Location.X + x, this.Location.Y + y);
                Location = newPoint;
            }
            public void ChangeState() 
            {
                if (isMoving)
                {
                    isMoving = false; Text = "OFF";
                    foreach (var cnt in controls)
                        cnt.Remove(this);
                }
                else 
                {
                    isMoving = true; Text = "ON";
                    foreach (var cnt in controls)
                        cnt.Add(this);
                }
            }
        }
        public class MyButton : Button, ISubscriber
        {
            private List<IObserver> observers = new List<IObserver>();
            public MyButton() 
            {
                Size = new Size(25, 25);
            }
            public void Add(IObserver observer) { observers.Add(observer); }
            public void Remove(IObserver observer) { observers.Remove(observer); }
            public void Move(int x, int y) 
            {
                foreach (var o in observers)
                    o.Update(this, x, y);
            }
        }

        public MainForm()
        {
            InitializeComponent();
        }
        private void InitializeControls(MainForm form, List<MyButton> controls, params MyButton[] buttons) 
        {
            foreach (var b in buttons)
                controls.Add(b);

            foreach (var c in controls)
                form.Controls.Add(c);
        }
        private void InitializeMovableButtons(MainForm form, int count) 
        {
            for (int i = 0; i < count; i++) 
            {
                MovableButton button = new MovableButton(i);
                button.Click += MovableButtonClickEvent;
                form.Controls.Add(button);
            }
        }
        void MovableButtonClickEvent(object sender, EventArgs e) 
        {
            var button = (MovableButton)sender;
            button.ChangeState();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            MyButton UP = new MyButton();
            UP.Location = new Point(80, 327);
            UP.Text = "U";

            MyButton DOWN = new MyButton();
            DOWN.Location = new Point(80, 356);
            DOWN.Text = "D";

            MyButton LEFT = new MyButton();
            LEFT.Location = new Point(38, 356);
            LEFT.Text = "L";

            MyButton RIGHT = new MyButton();
            RIGHT.Location = new Point(122,356);
            RIGHT.Text = "R";

            InitializeControls(this, controls, UP, DOWN, LEFT, RIGHT);
            InitializeMovableButtons(this, BUTTONS_COUNT);

            UP.Click    += buttonUPClick;
            DOWN.Click  += buttonDOWNClick;
            LEFT.Click  += buttonLEFTClick;
            RIGHT.Click += buttonRIGHTClick;
        }

        private void buttonUPClick(object sender, EventArgs e) 
        {
            var b = (MyButton)sender;
            b.Move(0, -3);
        }
        private void buttonDOWNClick(object sender, EventArgs e)
        {
            var b = (MyButton)sender;
            b.Move(0, 3);
        }
        private void buttonLEFTClick(object sender, EventArgs e)
        {
            var b = (MyButton)sender;
            b.Move(-3, 0);
        }
        private void buttonRIGHTClick(object sender, EventArgs e)
        {
            var b = (MyButton)sender;
            b.Move(3, 0);
        }
    }
}
