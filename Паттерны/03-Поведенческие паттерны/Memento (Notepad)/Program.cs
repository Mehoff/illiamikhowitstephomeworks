﻿using System;
using System.Collections.Generic;

namespace Memento
{
    class Program
    {
        public interface IOriginator 
        {
            object GetMemento();
            void SetMemento(object _memento);
        }

        public class Notepad : IOriginator 
        {
            private List<Caretaker> caretakers = new List<Caretaker>();
            private string text { get; set; }
            public Notepad() 
            {
                // Нулевое состояние тоже - состояние, добавляем;
                caretakers.Add(new Caretaker(this));
            }

            object IOriginator.GetMemento() 
            {
                return new Memento { text = this.text };
            }

            void IOriginator.SetMemento(object _memento) 
            {
                if (Object.ReferenceEquals(_memento, null))
                    throw new ArgumentNullException("memento");
                if (!(_memento is Memento))
                    throw new ArgumentException("memento");
                text = ((Memento)_memento).text;
            }

            public void AddText(string _text) 
            {
                text += _text;
                caretakers.Add(new Caretaker(this));
            }

            public void Undo() 
            {
                if (caretakers.Count >= 2)
                {
                    caretakers[caretakers.Count - 2].RestoreState(this);
                    caretakers.RemoveAt(caretakers.Count - 1);
                }
            }
            public string GetText() 
            {
                return text;
            }
            class Memento 
            {
                public string text { get; set; }
            }
        }
        public class Caretaker 
        {
            private object Memento;
            public Caretaker(IOriginator originator) 
            {
                SaveState(originator);
            }

            public void SaveState(IOriginator originator) 
            {
                if (originator != null)
                    Memento = originator.GetMemento();
                else throw new Exception("Caretaker::SaveState() - 'originator' is null");
            }

            public void RestoreState(IOriginator originator) 
            {
                if (originator == null)
                    throw new Exception("Caretaker::RestoreState() - 'originator' is null");
                if (Memento == null)
                    throw new Exception("Caretaker::RestoreState() - 'Memento' is null");
                originator.SetMemento(Memento);
            }
        }
        static void Main(string[] args)
        {
            bool isOn = true;
            Notepad note = new Notepad();

            while (isOn) 
            {
                Console.WriteLine($"Text:\n{note.GetText()}");
                Console.WriteLine("Any button - Add text | R - Undo");
                var action = Console.ReadKey().Key;
                switch (action) 
                {
                    case ConsoleKey.R:
                        note.Undo();
                        break;
                    default:
                        Console.WriteLine("Enter new text: ");
                        var newText = Console.ReadLine();
                        if (newText != null)
                            note.AddText(newText);
                        break;
                }
                Console.Clear();
            }
        }
    }
}
