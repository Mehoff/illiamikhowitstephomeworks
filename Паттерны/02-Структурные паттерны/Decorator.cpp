﻿//Decorator
#include <iostream>

struct Stats 
{
public:
	std::string name;
	int attack;
	int speed;
	int health;
	int armor;
	Stats() : name("undefined"),attack(0), speed(0), health(0), armor(0) 
	{}
};

class Player 
{
private:
	Stats stats;
public:
	virtual void Show() = 0;
	Stats& getStats() { return stats; }
};

class Human : public Player 
{
public:
	Human() 
	{

		std::cout << "\t<!> Human()\n";
		getStats().name = "Human";
		getStats().health += 150;
		getStats().armor += 0;
		getStats().attack += 20;
		getStats().speed += 20;
	}
	void Show() override 
	{
		std::cout << getStats().name << std::endl;
		std::cout << "Health: " << getStats().health << std::endl;
		std::cout << "Armor: " << getStats().armor << std::endl;
		std::cout << "Attack: " << getStats().attack << std::endl;
		std::cout << "Speed: " << getStats().speed << std::endl;
		std::cout << std::endl;
	}
};

class Elf : public Player
{
public:
	Elf()
	{
		std::cout << "\t<!> Elf()" << std::endl;

		getStats().name = "Elf";
		getStats().health += 100;
		getStats().armor += 0;
		getStats().attack += 15;
		getStats().speed += 30;
	}

	void Show() override
	{
		std::cout << getStats().name << std::endl;
		std::cout << "Health: " << getStats().health << std::endl;
		std::cout << "Armor: " << getStats().armor << std::endl;
		std::cout << "Attack: " << getStats().attack << std::endl;
		std::cout << "Speed: " << getStats().speed << std::endl;
		std::cout << std::endl;
	}
};

class Decorator : public Player
{
private:
	Player* player;
public:
	Decorator(Player * _player) : player(_player) {}
	~Decorator() { delete player; }

	void Show() override 
	{
		player->Show();
	}
	Player*& getPlayer() { return player; }
};

class HumanWarriorDecorator : public Decorator
{
public:
	HumanWarriorDecorator(Human * _player) : Decorator(_player) 
	{

		std::cout << "\t<!> HumanWarriorDecorator()\n";
		//?
		_player->getStats().name = "Human Warrior";
		this->getPlayer()->getStats().health += 50;
		this->getPlayer()->getStats().armor += 20;
		this->getPlayer()->getStats().attack += 20;
		this->getPlayer()->getStats().speed += 10;

	}

	void Show() override 
	{
		
		Decorator::Show();
	}

};

class HumanSwordsmanDecorator : public Decorator
{
public:
	HumanSwordsmanDecorator(HumanWarriorDecorator* _player) : Decorator(_player) 
	{

		std::cout << "\t<!> HumanSwordsmanDecorator()\n";
		_player->getPlayer()->getStats().name = "Human Swordsman";
		_player->getPlayer()->getStats().health += 50;
		_player->getPlayer()->getStats().armor += 40;
		_player->getPlayer()->getStats().attack += 40;
		_player->getPlayer()->getStats().speed -= 10;
	}

	void Show() override
	{
		Decorator::Show();
	}

};

class HumanArcherDecorator : public Decorator
{
public:
	HumanArcherDecorator(HumanWarriorDecorator* _player) : Decorator(_player) 
	{
		std::cout << "\t<!> HumanArcherDecorator()\n";


		_player->getPlayer()->getStats().name = "Human Archer";
		_player->getPlayer()->getStats().health += 50;
		_player->getPlayer()->getStats().armor += 10;
		_player->getPlayer()->getStats().attack += 20;
		_player->getPlayer()->getStats().speed += 20;
	}

	void Show() override
	{
		Decorator::Show();
	}

};

class HumanRiderDecorator : public Decorator
{
public:
	HumanRiderDecorator(HumanSwordsmanDecorator* _player) : Decorator(_player) 
	{
		std::cout << "\t<!> HumanRiderDecorator()\n";

		_player->getPlayer()->getStats().name = "Human Rider";
		_player->getPlayer()->getStats().health += 200;
		_player->getPlayer()->getStats().armor += 100;
		_player->getPlayer()->getStats().attack -= 10;
		_player->getPlayer()->getStats().speed += 40;
	}

	void Show() override
	{
		Decorator::Show();
	}

};

class ElfWarriorDecorator : public Decorator 
{
public:
	ElfWarriorDecorator(Elf * _player) : Decorator(_player)
	{
		std::cout << "\t<!> ElfWarriorDecorator()" << std::endl;

		_player->getStats().name = "Elf Warrior";
		this->getPlayer()->getStats().health += 100;
		this->getPlayer()->getStats().armor += 20;
		this->getPlayer()->getStats().attack += 20;
		this->getPlayer()->getStats().speed -= 10;

	}

	void Show() override
	{
		Decorator::Show();
	}
};

class ElfMagicianDecorator : public Decorator
{
public:
	ElfMagicianDecorator(Elf * _player) : Decorator(_player)
	{

		std::cout << "\t<!> ElfMagicianDecorator()" << std::endl;

	
		_player->getStats().name = "Elf Magician";
		this->getPlayer()->getStats().health -= 50;
		this->getPlayer()->getStats().armor += 10;
		this->getPlayer()->getStats().attack += 10;
		this->getPlayer()->getStats().speed += 10;


	}

	void Show() override
	{
		Decorator::Show();
	}
};

class ElfArbalesterDecorator : public Decorator
{
public:
	ElfArbalesterDecorator(ElfWarriorDecorator* _player) : Decorator(_player)
	{
		std::cout << "\t<!> ElfArbalesterDecorator()" << std::endl;

		_player->getPlayer()->getStats().name = "Elf Arbalester";
		_player->getPlayer()->getStats().health += 50;
		_player->getPlayer()->getStats().armor -= 10;
		_player->getPlayer()->getStats().attack += 20;
		_player->getPlayer()->getStats().speed += 10;	

	}

	void Show() override
	{
		Decorator::Show();
	}
};

class ElfEvilMagicianDecorator : public Decorator
{
public:
	ElfEvilMagicianDecorator(ElfMagicianDecorator* _player) : Decorator(_player)
	{
		std::cout << "\t<!>ElfEvilMagicianDecorator()" << std::endl;

		_player->getPlayer()->getStats().name = "Elf Evil Magician";
		_player->getPlayer()->getStats().health += 0;
		_player->getPlayer()->getStats().armor += 0;
		_player->getPlayer()->getStats().attack += 70;
		_player->getPlayer()->getStats().speed += 20;

	}

	void Show() override
	{
		Decorator::Show();
	}
};

class ElfLightMagicianDecorator : public Decorator
{
public:
	ElfLightMagicianDecorator(ElfMagicianDecorator* _player) : Decorator(_player)
	{

		std::cout << "\t<!>ElfLightMagicianDecorator()" << std::endl;

		_player->getPlayer()->getStats().name = "Elf Light Magician";
		_player->getPlayer()->getStats().health += 100;
		_player->getPlayer()->getStats().armor += 30;
		_player->getPlayer()->getStats().attack += 50;
		_player->getPlayer()->getStats().speed += 30;

	}

	void Show() override
	{
		Decorator::Show();
	}
};

int main()
{
	// - - - Humans

	Player* human = new Human();
	human->Show();

	Player* h_warrior = new HumanWarriorDecorator(new Human());
	h_warrior->Show();

	Player* h_archer = new HumanArcherDecorator(new HumanWarriorDecorator(new Human()));
	h_archer->Show();

	Player* h_swordsman = new HumanSwordsmanDecorator(new HumanWarriorDecorator(new Human()));
	h_swordsman->Show();
	
	
	std::cout << "- - - - - - - - - - - - - - - \n";

	// - - - Elfs

	Player* elf = new Elf();
	elf->Show();

	Player* e_warrior = new ElfWarriorDecorator(new Elf());
	e_warrior->Show();

	Player* e_magician = new ElfMagicianDecorator(new Elf());
	e_magician->Show();

	Player* e_arbalester = new ElfArbalesterDecorator(new ElfWarriorDecorator(new Elf()));
	e_arbalester->Show();	
	
	Player* e_evil_magician = new ElfEvilMagicianDecorator(new ElfMagicianDecorator(new Elf()));
	e_evil_magician->Show();
	
	Player* e_light_magician = new ElfLightMagicianDecorator(new ElfMagicianDecorator(new Elf()));
	e_light_magician->Show();



	



	

	

	


	return 0;
}
