﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleAppС_Sharp
{

    enum UnitType 
    {
        Infantry,
        Vehicle,
        HeavyVehicle,
        LightVehicle,
        Aircraft
    }
    public struct Vector2f 
    {

        public Vector2f(int _x, int _y) { x = _x; y = _y; }
        public int x { get; set; }
        public int y { get; set; }
    }

    abstract class Unit 
    {
        public Vector2f position;

        public UnitType type;
        public string name { get; set; }
        public int speed { get; set; }
        public int power { get; set; }

        public void Show(int x, int y) 
        {
            Console.WriteLine($"{name}\n(X: {x},Y: {y})\nPower: {power}\n");
        }

        public void setPosition(int x, int y) 
        {
            position.x = x;
            position.y = y;
        }
        public void setPosition(Vector2f pos) 
        {
            position = pos;
        }
    }

    class Infantry : Unit
    {
      
        public Infantry()
        {
            name = "Infantry";
            speed = 20;
            power = 10;
            type = UnitType.Infantry;
        }
    }
    class Vehicle : Unit
    {
        public Vehicle()
        {
            name = "Vehicle";
            speed = 70;
            power = 0;
            type = UnitType.Vehicle;
        }

    }
    class HeavyVehicle : Unit
    {
        public HeavyVehicle()
        {
            name = "Heavy Vehicle";
            speed = 15;
            power = 150;
            type = UnitType.HeavyVehicle;
        }

    }
    class LightVehicle : Unit
    {
        public LightVehicle()
        {
            name = "LightVehicle";
            speed = 50;
            power = 30;
            type = UnitType.LightVehicle;
        }

    }
    class Aircraft : Unit
    {
        public Aircraft()
        {
            name = "Aircraft";
            speed = 300;
            power = 100;
            type = UnitType.Aircraft;
        }

    }
    class UnitFactory 
    {
        private Dictionary<UnitType, Unit> existingUnitTypes = new Dictionary<UnitType, Unit>();
        public Unit setUnit(UnitType type, int x, int y) 
        {
            Unit unit = null;
            if (existingUnitTypes.ContainsKey(type))
            {
                unit = existingUnitTypes[type];
                unit.Show(x, y);
            }
            else 
            {
                switch (type) 
                {
                    case UnitType.Aircraft:     unit = new Aircraft();
                        break;
                    case UnitType.HeavyVehicle: unit = new HeavyVehicle();
                        break;
                    case UnitType.Infantry:     unit = new Infantry();
                        break;
                    case UnitType.LightVehicle: unit = new LightVehicle();
                        break;
                    case UnitType.Vehicle:      unit = new Vehicle();
                        break;
                    default: throw new Exception("Unexpected Unit Type");

                }

                existingUnitTypes.Add(type, unit);
                unit.Show(x, y);
            }

            return unit;
        }

    }


    class Program
    {
        static void Main(string[] args)
        {

            UnitFactory factory = new UnitFactory();
            factory.setUnit(UnitType.Aircraft,100,200);
            factory.setUnit(UnitType.Infantry, 123, 312);
            factory.setUnit(UnitType.Vehicle, 12, 423);
            factory.setUnit(UnitType.HeavyVehicle, 345, 123);
        }
    }
}



//Unit unit = null;

//            if (units.ContainsKey(_name))
//            {
//                unit = units[_name];
//            }
//            else 
//            {
//                switch (_name) 
//                {
//                    case "Infantary":     unit = new Infantry();
//                        break;
//                    case "Vehicle":       unit = new Vehicle();
//                        break;
//                    case "Heavy Vehicle": unit = new HeavyVehicle();
//                        break;
//                    case "Light Vehicle": unit = new LightVehicle();
//                        break;
//                    case "Aircraft":      unit = new Aircraft();
//                        break;
//                }
//                units.Add(_name, unit);
//            }
//            return unit;