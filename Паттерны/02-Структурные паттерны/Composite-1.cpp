#include <iostream>
#include <stdexcept>
#include <vector>


    //�� ������� ���� ������� �� ���������� ����������

std::string TabSize;

class Component 
{
private:
    std::string name;
public:

    virtual ~Component() {}

    virtual void addComponent(Component* _component) { throw std::exception("Can`t add to base Component"); }

    virtual void Show() {  }

    std::string& Name(){ return name; }
};


class OfficeComponent : public Component 
{
private: 
    std::vector<Component*> vec;
public:

    OfficeComponent(std::string _name) 
    {
        this->Name() = _name;
    }

    ~OfficeComponent() override
    {
        for (int i = 0; i < vec.size(); i++) delete vec[i];
    }

    void Show() override
    {
        std::cout << TabSize << Name() << std::endl;
        
        for (int i = 0; i < vec.size(); i++)
        {
            if (i != vec.size())
                TabSize += "\t";

            vec[i]->Show();
        }

        TabSize.clear();
    }
    
    void addComponent(Component* _component) override 
    {
        vec.push_back(_component);
    }

};


int main()
{

    setlocale(NULL, "Russian");

    std::vector<OfficeComponent*> components;
    std::vector<OfficeComponent*> components2;

    //      ��������

    components.push_back(new OfficeComponent("��������"));
    
    components.push_back(new OfficeComponent("������ ���"));        //1
    components[0]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("���������� ������")); //2
    components[0]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("������� 10 ����"));   //3
    components[2]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("������ �����"));      //4
    components[0]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("���� ���������"));    //5
    components[0]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("���������"));         //6
    components[5]->addComponent(components[components.size() - 1]);
    
    components.push_back(new OfficeComponent("������� HDD"));       //7
    components[6]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("������� ��������������")); //8
    components[5]->addComponent(components[components.size() - 1]);

    components.push_back(new OfficeComponent("����� � ������ � �������� �����")); //8
    components[0]->addComponent(components[components.size() - 1]);

    //      ���������

    components2.push_back(new OfficeComponent("���������"));

    components2.push_back(new OfficeComponent("10 ������"));        
    components2[0]->addComponent(components2[components2.size() - 1]);

    components2.push_back(new OfficeComponent("�����"));
    components2[0]->addComponent(components2[components2.size() - 1]);

    components2.push_back(new OfficeComponent("���� �������"));
    components2[0]->addComponent(components2[components2.size() - 1]);

    components2.push_back(new OfficeComponent("���������"));
    components2[3]->addComponent(components2[components2.size() - 1]);

    components2.push_back(new OfficeComponent("������� ������� �����������"));
    components2[0]->addComponent(components2[components2.size() - 1]);

    // Show

    components[0]->Show();

    std::cout << "\n\n- - - - - - - - - - - - - - \n\n";

    components2[0]->Show();
}
