﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    class Program
    {
        public interface PCComponent 
        {
            string Name { get; set; }
            void ShowInfo();
        }

        public class GraphicsCard : PCComponent 
        {
            public string Name { get; set; } = string.Empty;
            public GraphicsCard(string _name) 
            {
                Name = _name;
            }

            public void ShowInfo() 
            {
                Console.WriteLine($"Graphics Card Info: {Name}");
            }
        }
        public class CPU : PCComponent
        {
            public string Name { get; set; } = string.Empty;
            public CPU(string _name)
            {
                Name = _name;
            }

            public void ShowInfo()
            {
                Console.WriteLine($"CPU Info: {Name}");
            }
        }
        public class HardDrive : PCComponent
        {
            public string Name { get; set; } = string.Empty;
            public HardDrive(string _name)
            {
                Name = _name;
            }

            public void ShowInfo()
            {
                Console.WriteLine($"HardDrive Info: {Name}");
            }
        }

        public abstract class PCObject
        {
            public abstract void NextComponent();
            public abstract void PriorComponent();
            public abstract void AddComponent(PCComponent component);
            public abstract void DeleteRecord(PCComponent component);
            public abstract void ShowAllComponents();
            public abstract void ShowCurrent();
        }


        public class PCComponents
        {
            private readonly List<PCComponent> components = new List<PCComponent>();
            public int currentComponent { get; set; } = 0;
            public void Add(PCComponent component) { components.Add(component); }
            public void Remove(PCComponent component) { components.Remove(component); }
            public void Next()
            {
                if (currentComponent < components.Count - 1)
                    currentComponent++;
            }
            public void Prior() 
            {
                if (currentComponent > 0)
                    currentComponent--;
            }
            public PCComponent getCurrent() { return components[currentComponent]; }
            public List<PCComponent> getList() { return components; }

        }
        public class PC : PCObject 
        {
            PCComponents components = new PCComponents();

            public override void AddComponent(PCComponent component)
            {
                components.Add(component);
            }

            public override void DeleteRecord(PCComponent component)
            {
                components.Remove(component);
            }

            public override void NextComponent()
            {
                components.Next();
                ShowCurrent();
            }

            public override void PriorComponent()
            {
                components.Prior();
                ShowCurrent();
            }

            public override void ShowCurrent()
            {
                components.getCurrent().ShowInfo();
            }

            public override void ShowAllComponents()
            {
                foreach (var c in components.getList())
                    c.ShowInfo();
            }

            public string getCounterString() 
            {
                return $"{components.currentComponent + 1}/{components.getList().Count}";
            }
        }
        static void Main(string[] args)
        {
            bool isOn = true;
            PC pc = new PC();

            pc.AddComponent(new GraphicsCard("GTX 2020"));
            pc.AddComponent(new CPU("Intel i5 6600"));
            pc.AddComponent(new HardDrive("Seagate Barracuda"));

            do
            {
                Console.WriteLine($"({pc.getCounterString()}) <-\t->");
                var k = Console.ReadKey();
                Console.Clear();
                switch (k.Key)
                {
                    case ConsoleKey.LeftArrow:
                        pc.PriorComponent();
                        break;
                    case ConsoleKey.RightArrow:
                        pc.NextComponent();
                        break;
                    case ConsoleKey.Escape:
                        isOn = false;
                        break;
                    default:
                        break;
                }
            } while (isOn);
            pc.ShowAllComponents();
            Console.ReadKey();
        }
    }
}
