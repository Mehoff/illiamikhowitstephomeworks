﻿#include <iostream>

#define Print std::cout << 

class RAM;
class HardDrive;
class CDROM;
class PowerSupply;

class GPU;

class Sensor;

class Computer;



//RAM
class RAM
{
private:
    bool hasPower;
    std::string name;
    int capacity_mb;
public:
    RAM()
    {
        name = "Kingston Extreme Gaming";
        capacity_mb = 16 * 1024;
        hasPower = true;
    }
    void InitializeDevices() { Print "[RAM] Devices Initialized\n"; }
    void PowerUp() { hasPower = true; }
    bool MemoryCheck()
    {
        Print "[RAM] Memory Check...\n";
        Print "[RAM] Memory Complete!\n";
        return true;
    }

    bool& isPoweredUp() { return hasPower; }
    std::string& getName() { return name; }
    int& getCapacityMB() { return capacity_mb; }
};

//HARD DRIVE
class HardDrive
{
private:
    bool hasPower;
    std::string name;
    int capacity_mb;
public:
    HardDrive()
    {
        name = "Seagate Barracuda";
        capacity_mb = 2 * 1024 * 1000; //2TB
    }
    void PowerUp() { hasPower = true; }
    bool LoadSectorCheck()
    {
        Print "[HARD DRIVE] Sectors Check...\n";
        Print "[HARD DRIVE] Sectors Check Complete!\n";
        return true;
    }

    bool& isPoweredUp() { return hasPower; }
    std::string& getName() { return name; }
    int& getCapacityMB() { return capacity_mb; }
};

//CDROM
class CDROM
{
private:
    bool hasPower;
    std::string name;
public:
    CDROM()
    {
        name = "ASUS DVDRW 3000";
        hasPower = false;
    }
    void TurnOn() { hasPower = true; }
    bool isCDInside()
    {
        return true;
    }

    bool& isPoweredUp() { return hasPower; }
    std::string& getName() { return name; }
};


//GPU
class GPU
{
private:
    bool hasPower;
    std::string name;
    int ram;

public:

    GPU()
    {
        name = "ASUS RTX 2080TI";
        ram = 16 * 1024;
        hasPower = false;
    }

    void TurnOn();
    bool CheckIfMonitorConnected();
    void PrintRAMData(RAM& obj);
    void PrintCDROMData(CDROM& obj);
    void PrintHardDriveData(HardDrive& obj);

    bool& isPoweredUp() { return hasPower; }
    std::string& getName() { return name; }
    int& getRamCapacity() { return ram; }
};
void GPU::TurnOn() { hasPower = true; }
bool GPU::CheckIfMonitorConnected()
{
    Print "[GPU] Monitor Detected\n";
    return true;
}
void GPU::PrintRAMData(RAM& obj)
{
    Print "[GPU] RAM INFO:\n";
    Print obj.getName() << "\n";
    Print obj.getCapacityMB() << "MB\n";
}
void GPU::PrintCDROMData(CDROM& obj)
{
    Print "[GPU] CDROM INFO:\n";
    Print obj.getName() << "\n";
    obj.isCDInside() == true ? std::cout << "CD Detected!\n" : std::cout << "NO CD\n";

}
void GPU::PrintHardDriveData(HardDrive& obj)
{
    Print "[GPU] CDROM INFO:\n";
    Print obj.getName() << "\n";
    Print obj.getCapacityMB() << "MB\n";
}


//POWER SUPPLY
class PowerSupply
{
private:
    std::string name;
    bool hasPower;
    int WATT;
public:
    PowerSupply()
    {
        name = "GameMax GM-500B";
        hasPower = true;
        WATT = 500;
    }
    void PowerUp();
    void PowerUpCDROM(CDROM& obj);
    void PowerUpHardDrive(HardDrive& obj);
    void PowerUpGPU(GPU& obj);
    void PowerUpRAM(RAM& obj);

    bool& isPoweredUp() { return hasPower; }
    std::string& getName() { return name; }
    int& getWATT() { return WATT; }

};

void PowerSupply::PowerUp() { Print "[POWER SUPPLY] TURN ON\n"; hasPower = true; }
void PowerSupply::PowerUpCDROM(CDROM& obj)
{
    Print "[POWER SUPPLY] Powering up CDROM (" << obj.getName() << ")\n";
    if (!obj.isPoweredUp()) { obj.TurnOn(); }
}
void PowerSupply::PowerUpHardDrive(HardDrive& obj)
{
    Print "[POWER SUPPLY] Powering up HardDrive (" << obj.getName() << ")\n";
    if (!obj.isPoweredUp()) { obj.PowerUp(); }
}
void PowerSupply::PowerUpGPU(GPU& obj)
{
    Print "[POWER SUPPLY] Powering up GPU (" << obj.getName() << ")\n";
    if (!obj.isPoweredUp()) { obj.TurnOn(); }
}
void PowerSupply::PowerUpRAM(RAM& obj)
{
    Print "[POWER SUPPLY] Powering up RAM (" << obj.getName() << ")\n";
    if (!obj.isPoweredUp()) { obj.PowerUp(); }
}


class Sensor
{
public:
    Sensor() {}
    bool CheckAll(GPU& gpu, RAM& ram, HardDrive& drive, CDROM& rom, PowerSupply& power_supply);
    bool CheckPower(PowerSupply& obj);
    bool CheckGPUTemperature(GPU& obj);
    bool CheckPowerSupplyTemperature(PowerSupply& obj);
    bool CheckRAMTemperature(RAM& obj);
};
bool Sensor::CheckAll(GPU& gpu, RAM& ram, HardDrive& drive, CDROM& rom, PowerSupply& power_supply) 
{
    Print "[SENSOR] Global Hardware temperature test\n";
    if (gpu.isPoweredUp() && ram.isPoweredUp() && drive.isPoweredUp() && rom.isPoweredUp() && power_supply.isPoweredUp()) { Print "[SENSOR] OK\n"; return true; }
    else { Print "FAIL\n"; return false; }
}
bool Sensor::CheckPower(PowerSupply & obj)
{
    Print "[SENSOR] Power supply power check (" << obj.getName() << ")\n";

    if (obj.isPoweredUp())
    {
        Print "Has Power\n";
        return true;
    }
    else { Print "No power"; return false; }

}
bool Sensor::CheckGPUTemperature(GPU& obj) 
{
    Print "[SENSOR] GPU Temperature test (" << obj.getName() << ")\n";
    return true;
}
bool Sensor::CheckPowerSupplyTemperature(PowerSupply& obj) 
{
    Print "[SENSOR] Power Supply Temperature test (" << obj.getName() << ")\n";
    return true;
}
bool Sensor::CheckRAMTemperature(RAM& obj) 
{
    Print "[SENSOR] RAM Temperature test (" << obj.getName() << ")\n";
    return true;
}





class Computer 
{
private:
    bool state;
    bool isLoading = true;
    bool isLoadingSuccessful = false;
    Sensor sensor;
    GPU gpu;
    RAM ram;
    HardDrive drive;
    CDROM rom;
    PowerSupply power_supply;


    bool SensorsChecks() 
    {
        bool result = true;
        if (!sensor.CheckPower(power_supply)) { Print "Power Supply Power ERROR";  result = false; }
        if (!sensor.CheckGPUTemperature(gpu)) { Print "GPU Temperature ERROR"; result = false; }
        if (!sensor.CheckRAMTemperature(ram)) { Print "RAM Temperature ERROR"; result = false; }

        return result;
    }

public:
    Computer() 
    {
        sensor = Sensor();
        gpu = GPU();
        ram = RAM();
        drive = HardDrive();
        rom = CDROM();
        power_supply = PowerSupply();
    }

    void Spec() 
    {
        std::cout << "GPU: " << this->gpu.getName() << std::endl;
        std::cout << "RAM: " << this->ram.getName() << " /Capacity/: " << this->ram.getCapacityMB() << "MB" << std::endl;
        std::cout << "HardDrive: " << this->drive.getName() << " /Capacity/: " << this->drive.getCapacityMB() << "MB" << std::endl;
        std::cout << "PowerSupply: " << this->power_supply.getName() << " /WATT/: " << this->power_supply.getWATT() << "WATT" << std::endl;
        std::cout << "CDROM: " <<  this->rom.getName() << std::endl;
    }


    void TurnOn();
    void TurnOff() { Print "PC Turning off...";  state = false; }
    bool isTurnedOn() { return state; }
    
};

void Computer::TurnOn() 
{
  
    while (isLoading == true)
    {

        Print "/////////////////////\n";
        Print "PC is turning on...\n";
        power_supply.PowerUp();
        if (!SensorsChecks()) { isLoading = false; isLoadingSuccessful = false; }
        power_supply.PowerUpGPU(gpu);
        gpu.TurnOn();
        power_supply.PowerUpRAM(ram);
        ram.InitializeDevices();
        if (!ram.MemoryCheck()) { Print "Memory Check ERROR\n"; isLoading = false; isLoadingSuccessful = false; }
        gpu.PrintRAMData(ram);
        power_supply.PowerUpCDROM(rom);
        rom.TurnOn();
        bool cd = rom.isCDInside();
        gpu.PrintCDROMData(rom);
        power_supply.PowerUpHardDrive(drive);
        drive.PowerUp();
        if (!drive.LoadSectorCheck()) { Print "Hard Drive Loading Sector FAIL\n"; isLoading = false; isLoadingSuccessful = false;}
        gpu.PrintHardDriveData(drive);
        if (!sensor.CheckAll(gpu, ram, drive, rom, power_supply)) { Print "Temperature FAIL\n";  isLoading = false; isLoadingSuccessful = false; }

        if (cd == true) { Print "CD DISK founded in " << rom.getName(); }

        isLoading = false;
        isLoadingSuccessful = true;
    }
    if (isLoadingSuccessful) { Print "\n\nPC Boot OK"; state = true; }
    else { Print "\n\nPC Boot ERROR"; }
}

int main()
{
    Computer computer;

    computer.Spec();

    computer.TurnOn();
   
}


