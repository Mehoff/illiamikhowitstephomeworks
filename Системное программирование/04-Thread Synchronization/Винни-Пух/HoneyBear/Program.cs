﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// Винни-Пух и пчелы. Заданное количество пчел добывают мед равными порциями, 
// задерживаясь в пути на случайное время. Винни-Пух потребляет мед порциями заданной величины за заданное время
// и столько же времени может прожить без питания. Работа каждой пчелы реализуется в порожденном процессе (потоке).

namespace HoneyBear
{
    class Program
    {
        //Начальное количество мёда
        public const int BASIC_HONEY_COUNT = 100;

        //Медведь ест одинаковое количество за одинаковое время
        public const int BEAR_EAT_DELAY_MS = 1500;
        public const int BEAR_HONEY_EAT_COUNT = 15;

        //Пчела может приносить мёд в разный промежуток времени
        public const int MIN_ADD_HONEY_TIME = 500;
        public const int MAX_ADD_HONEY_TIME = 2000;

        //Пчела может принести больше или меньше мёда
        public const int MIN_ADD_HONEY_AMOUNT = 1;
        public const int MAX_ADD_HONEY_AMOUNT = 12;

        // Количество пчёл
        public const int BEES_COUNT = 10;

        private static object lockObj = new object();

        public static List<string> Log = new List<string>();

        public static void Show() 
        {
            lock (lockObj)
            {
                Console.Clear();
                foreach (var str in Log)
                    Console.WriteLine(str);

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"[ Кол-во мёда: {HoneyCount} ]");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        private static Random random = new Random();

        // Переменная к которой будет обращатся и медведь и каждая пчела
        private static int HoneyCount = BASIC_HONEY_COUNT;

        public class Bear
        {
            private Thread thread;
            public Bear() 
            {
                thread = new Thread(new ThreadStart(EatHoney));
                thread.IsBackground = false;
                thread.Start();
            }
            public void EatHoney() 
            {
                while (true)
                {
                    lock (lockObj) 
                    HoneyCount -= BEAR_HONEY_EAT_COUNT;

                    Thread.Sleep(BEAR_EAT_DELAY_MS);
                    Show();
                    Log.Add($"Винии съел {BEAR_HONEY_EAT_COUNT} мёда");
                }
            }
        }
        public class Bee
        {
            private Thread thread;
            public Bee()
            {
                thread = new Thread(new ThreadStart(AddHoney));
                thread.IsBackground = false;
                thread.Start();
            }
            public void AddHoney() 
            {
                while (true)
                {
                    if (HoneyCount >= 1000 || HoneyCount <= 0)
                    {
                      // Убить все потоки, и один раз показать экран завершения в зависимости от количества мёда
                      Thread.CurrentThread.Abort();
                    }

                   int sleep_value = random.Next(MIN_ADD_HONEY_TIME, MAX_ADD_HONEY_TIME);
                   int honeyCount  = random.Next(MIN_ADD_HONEY_AMOUNT, MAX_ADD_HONEY_AMOUNT);
                   
                   Thread.Sleep(sleep_value);
                   
                   lock (lockObj)     
                   HoneyCount += honeyCount;
                   
                   Log.Add($"Пчела работала {sleep_value} мс. и принесла {honeyCount} мёда");

                   Show();
                }
            }
        }

        static void Main(string[] args)
        {
            List<Bee> bees = new List<Bee>();
            for(int i = 0; i < 10; i++)
            bees.Add(new Bee());

            Bear Vinny = new Bear();
        }
    }
}
