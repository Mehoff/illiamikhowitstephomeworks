﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Airplanes
{
    class Program
    {

        public static Random rand = new Random();
        public static Stopwatch watch = new Stopwatch();
        public static object lockObj = new object();

        const int SEEK_TIME_SEC = 5;
        const int MATRIX_WIDTH = 20;
        const int MATRIX_HEIGHT = 20;
        public class Matrix 
        {
            public Seeker[] seekers = new Seeker[10];
            public static char[,] board = new char[MATRIX_WIDTH, MATRIX_HEIGHT];
            public Matrix() 
            {
                for (int i = 0; i < 10; i++)
                    seekers[i] = new Seeker();

                Initialize();
                Start();
            }
            private void Initialize() 
            {
                for (int i = 0; i < MATRIX_HEIGHT; i++) 
                {
                    for (int j = 0; j < MATRIX_WIDTH; j++) 
                    {
                        board[i, j] = getRandomChar();
                    }
                }
            }
            private char getRandomChar() 
            {
                var val = rand.Next(0, 2);
                if (val == 0)
                    return '0';
                else return '1';
            }
            public void Start() 
            {
                watch.Start();
                int _val = 1;
                foreach (var seeker in seekers) 
                {
                    seeker.thread = new Thread(new ParameterizedThreadStart(seeker.Seek));
                    seeker.thread.Name = $"Seeker #{_val}";
                    seeker.thread.Start(board);
                    _val++;
                }
            }
        }
        public class Seeker 
        {
            public char ch = 'X';
            int x;
            int y;
            public Thread thread;
            public Seeker() 
            {
                x = rand.Next(0, 19);
                y = rand.Next(0, 19);
            }
            public void Move() 
            {
                int val = rand.Next(0, 3);

                switch (val) 
                {
                    // Up
                    case 0: 
                        {
                            if (y != 0)
                                y--;
                            else Move();
                        }
                        break;
                    // Down
                    case 1:
                        {
                            if (y != 19)
                                y++;
                            else Move();
                        }
                        break;
                    // Right
                    case 2:
                        {
                            if (x != 19)
                                x++;
                            else Move();
                        }
                        break;
                    // Left
                    case 3:
                        {
                            if (x != 0)
                                x--;
                            else Move();
                        }
                        break;
                }
            }
            public void Seek(object _board) 
            {
                while (watch.ElapsedMilliseconds <= SEEK_TIME_SEC * 1000)
                {
                    lock (lockObj)
                    {
                        char[,] __board = (char[,])_board;
                        Move();
                        if (__board[x, y] == '1')
                            found++;
                        __board[x, y] = ch;

                        Show(__board);
                    }
                    Thread.Sleep(rand.Next(300, 500));
                }
                thread.Abort();
            }
            private void Show(char[,] _board) 
            {
                Console.Clear();
                for (int i = 0; i < MATRIX_HEIGHT; i++)
                {
                    Console.WriteLine();
                    for (int j = 0; j < MATRIX_WIDTH; j++)
                    {
                        if (_board[i, j] == 'X')
                            Console.ForegroundColor = ConsoleColor.Red;
                        else Console.ForegroundColor = ConsoleColor.White;
                        Console.Write(_board[i, j]);
                    }
                }
            }
            public int found { get; private set; } = 0;
        }
        static void Main(string[] args)
        {
            Matrix matrix = new Matrix();

            Console.ReadKey();

            Console.WriteLine("\n\n- - - - - - - - - - - - - - - -");
            foreach(var seeker in matrix.seekers)
                Console.WriteLine($"{seeker.thread.Name} - {seeker.found}");

            Console.ReadKey();
        }
    }
}
