﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WordsFilter
{
    public static class Report
    {
        private static Dictionary<string, int> wordsRateDictionary = new Dictionary<string, int>();
        public static void AddWord(string word) 
        {
            // Если это слово еще не встречалось, добавляем его в словарь
            if (!wordsRateDictionary.ContainsKey(word)) 
                wordsRateDictionary.Add(word, 1);

            // Если это слово уже есть в словаре - прибавляем Value
            else  wordsRateDictionary[$"{word}"]++; 
        }
        public static void AddWord(string word, int count)
        {
            // Если это слово еще не встречалось, добавляем его в словарь
            if (!wordsRateDictionary.ContainsKey(word))
                wordsRateDictionary.Add(word, count);

            // Если это слово уже есть в словаре - прибавляем Value
            else wordsRateDictionary[$"{word}"]+=count;
        }
        public static void SaveReport() 
        {
            int counter = 1;
            StringBuilder reportString = new StringBuilder();
            reportString.Append($"[{DateTime.Now.ToString("g")}]\n");
            foreach (KeyValuePair<string, int> pair in wordsRateDictionary.OrderBy(key => key.Value)) 
            {
                reportString.Append($"{counter}.{pair.Key} - {pair.Value}");
                counter++;
            }
            reportString.Append("\n- - - - - - - - - - - - - - - -\n");


            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\WordsFilterReport.txt";

            using (StreamWriter sw = File.AppendText(path)) 
            {
                sw.WriteLine(reportString.ToString());
            }
            MessageBox.Show("Отчет о проделанной работе сохранён в папке 'Мои документы'");

        }
        public static string GetString() 
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in wordsRateDictionary) 
            {
                builder.Append($"{item.Key} - {item.Value}\n");
            }
            return builder.ToString();
        }
        public static void Clear() 
        {
            wordsRateDictionary.Clear();
        }
    }
}
