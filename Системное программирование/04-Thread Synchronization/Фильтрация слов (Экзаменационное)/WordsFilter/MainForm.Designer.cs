﻿namespace WordsFilter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserFrom = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserTo = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileWords = new System.Windows.Forms.OpenFileDialog();
            this.textBoxFrom = new System.Windows.Forms.TextBox();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.buttonFrom = new System.Windows.Forms.Button();
            this.buttonTo = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.progressBarFiltering = new System.Windows.Forms.ProgressBar();
            this.labelPathFrom = new System.Windows.Forms.Label();
            this.labelPathTo = new System.Windows.Forms.Label();
            this.groupBoxFilters = new System.Windows.Forms.GroupBox();
            this.buttonChooseFilterFile = new System.Windows.Forms.Button();
            this.textBoxUserInput = new System.Windows.Forms.TextBox();
            this.radioButtonChooseFilterFile = new System.Windows.Forms.RadioButton();
            this.radioButtonUserInput = new System.Windows.Forms.RadioButton();
            this.groupBoxFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileWords
            // 
            this.openFileWords.FileName = "openFileDialog1";
            this.openFileWords.Filter = "Text files|*.txt";
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.Location = new System.Drawing.Point(12, 60);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.ReadOnly = true;
            this.textBoxFrom.Size = new System.Drawing.Size(205, 20);
            this.textBoxFrom.TabIndex = 0;
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(279, 60);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.ReadOnly = true;
            this.textBoxTo.Size = new System.Drawing.Size(205, 20);
            this.textBoxTo.TabIndex = 0;
            // 
            // buttonFrom
            // 
            this.buttonFrom.Location = new System.Drawing.Point(12, 87);
            this.buttonFrom.Name = "buttonFrom";
            this.buttonFrom.Size = new System.Drawing.Size(205, 23);
            this.buttonFrom.TabIndex = 1;
            this.buttonFrom.Text = "Откуда...";
            this.buttonFrom.UseVisualStyleBackColor = true;
            this.buttonFrom.Click += new System.EventHandler(this.buttonFrom_Click);
            // 
            // buttonTo
            // 
            this.buttonTo.Location = new System.Drawing.Point(279, 87);
            this.buttonTo.Name = "buttonTo";
            this.buttonTo.Size = new System.Drawing.Size(205, 23);
            this.buttonTo.TabIndex = 1;
            this.buttonTo.Text = "Куда...";
            this.buttonTo.UseVisualStyleBackColor = true;
            this.buttonTo.Click += new System.EventHandler(this.buttonTo_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(279, 290);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(204, 23);
            this.buttonStop.TabIndex = 1;
            this.buttonStop.Text = "Стоп";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(13, 290);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(204, 23);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Начать";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // progressBarFiltering
            // 
            this.progressBarFiltering.Location = new System.Drawing.Point(13, 261);
            this.progressBarFiltering.Name = "progressBarFiltering";
            this.progressBarFiltering.Size = new System.Drawing.Size(470, 23);
            this.progressBarFiltering.TabIndex = 2;
            // 
            // labelPathFrom
            // 
            this.labelPathFrom.AutoSize = true;
            this.labelPathFrom.Location = new System.Drawing.Point(12, 44);
            this.labelPathFrom.Name = "labelPathFrom";
            this.labelPathFrom.Size = new System.Drawing.Size(131, 13);
            this.labelPathFrom.TabIndex = 3;
            this.labelPathFrom.Text = "Путь к папке с файлами";
            // 
            // labelPathTo
            // 
            this.labelPathTo.AutoSize = true;
            this.labelPathTo.Location = new System.Drawing.Point(276, 44);
            this.labelPathTo.Name = "labelPathTo";
            this.labelPathTo.Size = new System.Drawing.Size(153, 13);
            this.labelPathTo.TabIndex = 3;
            this.labelPathTo.Text = "Путь сохранения результата";
            // 
            // groupBoxFilters
            // 
            this.groupBoxFilters.Controls.Add(this.buttonChooseFilterFile);
            this.groupBoxFilters.Controls.Add(this.textBoxUserInput);
            this.groupBoxFilters.Controls.Add(this.radioButtonChooseFilterFile);
            this.groupBoxFilters.Controls.Add(this.radioButtonUserInput);
            this.groupBoxFilters.Location = new System.Drawing.Point(13, 127);
            this.groupBoxFilters.Name = "groupBoxFilters";
            this.groupBoxFilters.Size = new System.Drawing.Size(470, 128);
            this.groupBoxFilters.TabIndex = 4;
            this.groupBoxFilters.TabStop = false;
            this.groupBoxFilters.Text = "Выбор слов для фильтрации...";
            // 
            // buttonChooseFilterFile
            // 
            this.buttonChooseFilterFile.Location = new System.Drawing.Point(6, 76);
            this.buttonChooseFilterFile.Name = "buttonChooseFilterFile";
            this.buttonChooseFilterFile.Size = new System.Drawing.Size(458, 23);
            this.buttonChooseFilterFile.TabIndex = 2;
            this.buttonChooseFilterFile.Text = "Выбрать файл-фильтр...";
            this.buttonChooseFilterFile.UseVisualStyleBackColor = true;
            this.buttonChooseFilterFile.Click += new System.EventHandler(this.buttonChooseFilterFile_Click);
            // 
            // textBoxUserInput
            // 
            this.textBoxUserInput.Location = new System.Drawing.Point(6, 19);
            this.textBoxUserInput.Name = "textBoxUserInput";
            this.textBoxUserInput.Size = new System.Drawing.Size(458, 20);
            this.textBoxUserInput.TabIndex = 1;
            // 
            // radioButtonChooseFilterFile
            // 
            this.radioButtonChooseFilterFile.AutoSize = true;
            this.radioButtonChooseFilterFile.Location = new System.Drawing.Point(6, 105);
            this.radioButtonChooseFilterFile.Name = "radioButtonChooseFilterFile";
            this.radioButtonChooseFilterFile.Size = new System.Drawing.Size(201, 17);
            this.radioButtonChooseFilterFile.TabIndex = 0;
            this.radioButtonChooseFilterFile.TabStop = true;
            this.radioButtonChooseFilterFile.Text = "Открыть txt файл (через запятную)";
            this.radioButtonChooseFilterFile.UseVisualStyleBackColor = true;
            this.radioButtonChooseFilterFile.CheckedChanged += new System.EventHandler(this.radioButtonChooseFilterFile_CheckedChanged);
            // 
            // radioButtonUserInput
            // 
            this.radioButtonUserInput.AutoSize = true;
            this.radioButtonUserInput.Location = new System.Drawing.Point(6, 45);
            this.radioButtonUserInput.Name = "radioButtonUserInput";
            this.radioButtonUserInput.Size = new System.Drawing.Size(231, 17);
            this.radioButtonUserInput.TabIndex = 0;
            this.radioButtonUserInput.TabStop = true;
            this.radioButtonUserInput.Text = "Ввести самостоятельно (через запятую)";
            this.radioButtonUserInput.UseVisualStyleBackColor = true;
            this.radioButtonUserInput.CheckedChanged += new System.EventHandler(this.radioButtonUserInput_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 325);
            this.Controls.Add(this.groupBoxFilters);
            this.Controls.Add(this.labelPathTo);
            this.Controls.Add(this.labelPathFrom);
            this.Controls.Add(this.progressBarFiltering);
            this.Controls.Add(this.buttonTo);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonFrom);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.textBoxFrom);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск запрещенных слов";
            this.groupBoxFilters.ResumeLayout(false);
            this.groupBoxFilters.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserFrom;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserTo;
        private System.Windows.Forms.OpenFileDialog openFileWords;
        private System.Windows.Forms.TextBox textBoxFrom;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.Button buttonFrom;
        private System.Windows.Forms.Button buttonTo;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.ProgressBar progressBarFiltering;
        private System.Windows.Forms.Label labelPathFrom;
        private System.Windows.Forms.Label labelPathTo;
        private System.Windows.Forms.GroupBox groupBoxFilters;
        private System.Windows.Forms.Button buttonChooseFilterFile;
        private System.Windows.Forms.TextBox textBoxUserInput;
        private System.Windows.Forms.RadioButton radioButtonChooseFilterFile;
        private System.Windows.Forms.RadioButton radioButtonUserInput;
    }
}

