﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordsFilter
{
    public partial class MainForm : Form
    {
        private CancellationTokenSource cts; 
        private CancellationToken ct;
        private Task[] tasks;
        private Filter filter;
        private static object lockObject = new object();
        private Semaphore semaphore;
        private string pathFrom;
        private string pathTo;

        public MainForm()
        {
            InitializeComponent();
            Initialize();
        }
        private void Initialize() 
        {
            radioButtonChooseFilterFile.CheckedChanged += radioButtonChooseFilterFile_CheckedChanged;
            radioButtonUserInput.CheckedChanged += radioButtonChooseFilterFile_CheckedChanged;
            radioButtonUserInput.Checked = true;
            buttonStop.Enabled = false;
            semaphore = new Semaphore(5, 5);
            filter = new Filter();
            cts = new CancellationTokenSource();
            ct = cts.Token;
        }
        
        //Button Events:

        private void buttonFrom_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserFrom.ShowDialog();

            if (result == DialogResult.OK) 
            {
                pathFrom = folderBrowserFrom.SelectedPath;
                textBoxFrom.Text = pathFrom;
            }
        }
        private void buttonTo_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserTo.ShowDialog();

            if(result == DialogResult.OK) 
            {
                pathTo = folderBrowserTo.SelectedPath;
                textBoxTo.Text = pathTo;
            }
        }
        private void buttonChooseFilterFile_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileWords.ShowDialog();

            if (result == DialogResult.OK) 
            {
                filter.Clear();
            }
        }
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (isFieldsFilled())
                StartWorking();
        }
        private void buttonStop_Click(object sender, EventArgs e){
            cts.Cancel();

            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }
        private void radioButtonUserInput_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonUserInput.Checked)
                textBoxUserInput.Enabled = true;
            else 
                textBoxUserInput.Enabled = false;
        }
        private void radioButtonChooseFilterFile_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonChooseFilterFile.Checked)
                buttonChooseFilterFile.Enabled = true;
            else
                buttonChooseFilterFile.Enabled = false;
        }
       
        //Other:
      
        private void StartWorking() 
        {
            progressBarFiltering.BeginInvoke((MethodInvoker)(() =>
            progressBarFiltering.Value = 0
                ));

            LoadFilterWords();
            Report.Clear();

            var txtFilePaths = GetFileStrings();

            tasks = new Task[txtFilePaths.Length];
            progressBarFiltering.Maximum = txtFilePaths.Length;

            buttonStop.Enabled = true;
            buttonStart.Enabled = false;

            for (int i = 0; i < txtFilePaths.Length; i++) 
            {
                var current = i;
                tasks[current] = new Task(() => DoWork(txtFilePaths[current], ct));
            }
            for (int i = 0; i < tasks.Length; i++)
            {
                var current = i;
                tasks[i].Start();
            }

            buttonStop.Enabled = false;
            buttonStart.Enabled = true;
         
        }
        private void DoWork(object path, CancellationToken token) 
        {
            semaphore.WaitOne();

            var text = File.ReadAllText((string)path, Encoding.Default);
            
            foreach (var badWord in filter.words) 
            {
                if (token.IsCancellationRequested)
                    return;

                int amount = new Regex(badWord).Matches(text).Count;

                lock (lockObject) 
                     Report.AddWord(badWord, amount);

                text = text.Replace(badWord, "*******");
            }

            byte[] bytes = Encoding.Default.GetBytes(text);
            try
            {
                File.WriteAllBytes($"{pathTo}\\{GetFileNameOnly((string)path)}", bytes);
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
            progressBarFiltering.BeginInvoke((MethodInvoker)(() =>
                progressBarFiltering.Value++
            ));

            if (tasks.Length - tasks.Count(x => x.IsCompleted) == 1)
                Report.SaveReport();

            semaphore.Release();
        }
        private string GetFileNameOnly(string path) 
        {
            StringBuilder newStr = new StringBuilder();
            char[] arr = path.ToCharArray();
            for (int i = arr.Length - 1; arr[i] != '\\'; i--) 
            {
              newStr.Insert(0, arr[i]);
            }
            return newStr.ToString();
        }
        private void LoadFilterWords() 
        {
            if (radioButtonChooseFilterFile.Checked)
                filter.AddWordsFromTxtFile(openFileWords.FileName);

            else filter.AddWordsFromString(textBoxUserInput.Text);
        }
        private string[] GetFileStrings() 
        {
            return Directory.GetFiles(pathFrom, "*txt");
        }
        private bool isFieldsFilled()
        {
            if (!string.IsNullOrEmpty(textBoxTo.Text) && !string.IsNullOrEmpty(textBoxFrom.Text)) return true;
            else return false;
        }
    }
}
