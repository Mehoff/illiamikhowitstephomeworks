﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WordsFilter
{
    class Filter
    {
        public List<string> words { get; private set; }
        public Filter() { words = new List<string>(); }
        public void AddWordsFromString(string str) 
        {
            if (str != string.Empty)
                AddWords(str.Trim().Split(','));
        }
        public void AddWordsFromTxtFile(string path) 
        {
            if (File.Exists(path)) 
            {
                string text = File.ReadAllText(path, Encoding.Default);
                AddWordsFromString(text);
            }
            else 
            {
                throw new Exception($"File is not existing by path: {path}");
            }
        }
        public void Clear() 
        {
            words.Clear();
        }
        //Private:
        private void AddWords(params string[] strArray) 
        {
            foreach (string str in strArray)
                words.Add(str);
        }
    }
}
