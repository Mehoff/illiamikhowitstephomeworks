﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Circles
{
    //ToDo Colors

    class Program
    {
        public const char CIRCLE = 'o';
        public const int WINDOW_WIDTH = 50;
        public const int WINDOW_HEIGHT = 30;
        public const int CIRCLE_COUNT = 15;

        public static Random random = new Random();
        public static object lockObject = new object();
        struct Vector2i 
        {
            public int x { get; set; }
            public int y { get; set; }

            public Vector2i(int _x, int _y)
            {
                x = _x;
                y = _y;
            }
        }
        class Circle 
        {
            private Thread thread;
            private Vector2i position;
            // Color
            public Circle() : this(new Vector2i(random.Next(1,WINDOW_WIDTH), 0)) { }
            public Circle(int x, int y) : this(new Vector2i(x, y)) { }
            public Circle(Vector2i _position) 
            {
                position.x = _position.x;
                position.y = _position.y;

                thread = new Thread(new ThreadStart(Print));
                thread.IsBackground = false;
                thread.Start();
            }
            public void Print() 
            {
                while (true)
                {
                    lock (lockObject)
                    {
                        //Clear last position space
                        Console.SetCursorPosition(position.x, position.y);
                        Console.Write(" ");

                        //Change position
                        ChangeX();
                        position.y++;

                        //Check Border Cross
                        if (CheckBorderCross())
                            ResetPositionY();

                        //Drawing on new position
                        Console.SetCursorPosition(position.x, position.y);
                        setRandomColor();
                        Console.Write(CIRCLE);

                    }
                    // Sleep
                    Thread.Sleep(random.Next(100, 300));
                }
                    
            }
            private void ChangeX()
            {
                if (position.x == 0)
                    position.x++;
                else if (position.x == WINDOW_WIDTH)
                    position.x--;
                else { position.x += random.Next(-1, 2); }
            }
            private void ResetPositionY() 
            {
                position.y = 0;
            }
            private bool CheckBorderCross() 
            {
                if (position.y == WINDOW_HEIGHT - 1)
                    return true;
                else return false;
            }
            private void setRandomColor() 
            {
                Console.ForegroundColor = (ConsoleColor)random.Next(1, 15);
            }

        }
        static void Main(string[] args)
        {
            Console.SetWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
            Console.CursorVisible = false;

            List<Circle> circles = new List<Circle>();

            for (int i = 1; i < CIRCLE_COUNT; i++)
                circles.Add(new Circle());
        }
    }
}
