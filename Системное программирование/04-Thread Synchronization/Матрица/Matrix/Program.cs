﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matrix
{
	class Program
	{
		private static Random _Random = new Random();

		// Constants:

		private static char[] CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ^&$%#@".ToCharArray();
		//
		private const int STRINGS_COUNT = 100;
		//
		private const int WINDOW_WIDTH = 200;
		private const int WINDOW_HEIGHT = 50;
		//
		private const int ZERO = 0;
		private const int MAX_STRING_LENGTH = 8;
		private const int MIN_STRING_LENGTH = 3;
		private const int MAX_X_COORDINATE = 40;
		private const int MIN_SLEEP_VALUE = 100;
		private const int MAX_SLEEP_VALUE = 300;

		private static object lockObject = new object();

		public struct MatrixChar
		{
			//Position in console
			public int x, y;
			//Character
			public char Char { get; set; }
		}

		public class MatrixString
		{
			private int sleepMS;
			private MatrixChar[] chars;
			private int X = ZERO;

			public Thread thread;

			public MatrixString()
			{
				X = _Random.Next(ZERO, MAX_X_COORDINATE);
				GenerateString();
				sleepMS = _Random.Next(MIN_SLEEP_VALUE, MAX_SLEEP_VALUE);

				thread = new Thread(new ThreadStart(Move));
				thread.IsBackground = false;
				thread.Start();
			}
			public MatrixString(int _X)
			{
				X = _X;
				GenerateString();
				sleepMS = _Random.Next(MIN_SLEEP_VALUE, MAX_SLEEP_VALUE);

				thread = new Thread(new ThreadStart(Move));
				thread.IsBackground = false;
				thread.Start();
			}
			private void GenerateString()
			{
				chars = new MatrixChar[_Random.Next(MIN_STRING_LENGTH, MAX_STRING_LENGTH)];

				for (int i = 0; i < chars.Length; i++)
				{
					chars[i].Char = GetRandomCharacter();
					chars[i].x = X;
					chars[i].y = i;
				}
			}
			private char GetRandomCharacter()
			{
				return CHARS[_Random.Next(ZERO, CHARS.Length)];
			}
			public void Move()
			{
				while (true)
				{
					lock (lockObject)
					{
						for (int i = ZERO; i < chars.Length; i++)
						{
							Console.SetCursorPosition(chars[i].x, chars[i].y);
							Console.Write(" ");
						}

						for (int i = ZERO; i < chars.Length; i++)
						{

							if (i == chars.Length - 1)
								Console.ForegroundColor = ConsoleColor.White;
							else if (i == chars.Length - 2)
								Console.ForegroundColor = ConsoleColor.Green;
							else
								Console.ForegroundColor = ConsoleColor.DarkGreen;

							if (chars[chars.Length - 1].y >= WINDOW_HEIGHT - 1)
								GenerateString();

							Console.SetCursorPosition(chars[i].x, ++chars[i].y);
							Console.Write(chars[i].Char);
						}
					}
					Thread.Sleep(sleepMS);
				}
			}
			public string GetString()
			{
				string str = string.Empty;
				for (int i = ZERO; i < chars.Length; i++)
					str += chars[i].Char;

				return str;
			}
		}

		static void Main(string[] args)
		{
			Console.SetWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
			Console.CursorVisible = false;

			List<MatrixString> strs = new List<MatrixString>();

			for (int i = ZERO; i < STRINGS_COUNT; i++)
				strs.Add(new MatrixString(i));
		}
	}
}
