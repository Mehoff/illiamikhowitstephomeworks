﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadsThirdExcercie
{
    public partial class Form2 : Form
    {
        public Thread thread;
        public Form2()
        {
     
            InitializeComponent();

            thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }

        private void Run() 
        {
            for (int i = 1; i <= 20; i++) 
            {
                Thread.Sleep(500);
                ChangeLabelOneTextSafe(i);
            }
            Thread.Sleep(500);
            BeginInvoke((MethodInvoker)(() => Close()));
        }


        private void ChangeLabelOneTextSafe(int value) 
        {
            if (InvokeRequired)
                BeginInvoke(new Action<int>(v => { SetLableOneText(v); }), value);
            else
                SetLableOneText(value);
        }
        private void SetLableOneText(int value) 
        {
            label1.Text = value.ToString();
        }
    }
}
