﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadsThirdExcercie
{
    public partial class Form1 : Form
    {
        public Form1()
        {
           // Control.CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }
  
        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Text = "Не модальное окно";
            form2.Show();
        }
    }
}
