﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadTimer
{
    public partial class MainForm : Form
    {
        int[] counters = new int[3] { 0, 0, 0 };
        Thread[] threads = new Thread[3];

        public MainForm()
        {
            InitializeComponent();
            threads[0] = new Thread(new ThreadStart(RunFirst));
            threads[1] = new Thread(new ThreadStart(RunSecond));
            threads[2] = new Thread(new ThreadStart(RunThird));
        }
        private void RunFirst()
        {
            while (true)
            {
                Thread.Sleep(500);
                counters[0]++;
                setLabel1TextSafe(counters[0].ToString());
            }
        }
        private void setLabel1TextSafe(string text)
        {
            if (InvokeRequired)
                BeginInvoke(new Action<string>(s => { setLabel1Text(s); }), text);
        }
        private void setLabel1Text(string text)
        {

            label1.Text = text;
        }

        private void RunSecond()
        {
            while (true)
            {
                Thread.Sleep(1000);
                counters[1]++;
                setLabel2TextSafe(counters[1].ToString());
            }
        }
        private void setLabel2TextSafe(string text)
        {
            if (InvokeRequired)
                BeginInvoke(new Action<string>(s => { setLabel2Text(s); }), text);
        }
        private void setLabel2Text(string text)
        {
            label2.Text = text;
        }


        private void RunThird()
        {
            while (true)
            {
                Thread.Sleep(1500);
                counters[2]++;
                setLabel3TextSafe(counters[2].ToString());
            }
        }

        private void setLabel3TextSafe(string text) 
        {
            if (InvokeRequired)
                BeginInvoke(new Action<string>(s => { setLabel3Text(s); }), text);
        }
        private void setLabel3Text(string text) 
        {
            label3.Text = text;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            foreach(Thread th in threads) { th.Start(); }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            foreach (Thread th in threads) { th.Abort(); }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            label1.Text = "0";
            label2.Text = "0";
            label3.Text = "0";
            counters = new int[3] { 0, 0, 0 };
        }
    }
}
