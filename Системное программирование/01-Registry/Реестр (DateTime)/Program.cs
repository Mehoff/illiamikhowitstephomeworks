﻿using System;
using System.ComponentModel.Design;
using Microsoft.Win32;

namespace RegApp_1
{

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\MehSoft\UselessApp"))
                {

                    if (key != null)
                    {
                        object o = key.GetValue("Last Open");
                        if (o != null)
                        {
                            Console.WriteLine("Last time opened on: " + o.ToString());
                        }
                        else { Console.WriteLine("Launching for the first time"); }
                    }

                    DateTime localtime = DateTime.Now;

                    Console.WriteLine("Writing new DateTime Value: " + localtime.ToString());
                   
                    key.SetValue("Last Open", localtime);
                }

                using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run")) 
                {
                    if (key != null) 
                    {
                        key.SetValue("UselessApp", @"E:\Code\Visual Studio Projects\RegApp_1\obj\Debug\RegApp_1.exe");
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
