﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Xml.Linq;
using Microsoft.Win32;

namespace RegApp_1
{

    class Program
    {
        const string myKeyPath = @"Software\MehSoft\UselessApp";
        static class Regedit 
        {
            static public void WithCount(List<string> list)
            {
                try
                {
                    using (RegistryKey key = Registry.CurrentUser.CreateSubKey(myKeyPath))
                    {
                        Console.WriteLine("SAVING values from List USING count value...");
                        
                        key.SetValue("Count", list.Count);
                        for (int i = 0; i < list.Count; i++) { key.SetValue($"Item_{i}", list[i]); }

                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }

                try
                {
                    using (RegistryKey key = Registry.CurrentUser.OpenSubKey(myKeyPath))
                    {
                        Console.WriteLine("READING values from List USING count value...");

                        for (int i = 0; i < Convert.ToInt32(key.GetValue("Count")); i++) 
                        {
                            Console.WriteLine(key.GetValue($"Item_{i}"));
                        }
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }

            }

            static public void WithoutCount(List<string> list)
            {
                try
                {
                    using (RegistryKey key = Registry.CurrentUser.CreateSubKey(myKeyPath))
                    {
                        Console.WriteLine("SAVING values from List WITHOUT count value...");
                        int val = 0;
                        foreach (var obj in list)
                        {
                            key.SetValue($"{obj}", obj); val++;
                        }
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }

                try
                {
                    using (RegistryKey key = Registry.CurrentUser.OpenSubKey(myKeyPath))
                    {
                        Console.WriteLine("READING values from List WITHOUT count value...");
                        
                        foreach(var item in key.GetValueNames())
                            Console.WriteLine(item.ToString());
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            }

        }
        static void Main(string[] args)
        {

            //Console.WriteLine("With Count/Without count ? (1/2): ");

            //List<string> list = new List<string>();
            //for(int i = 0; i < 3; i++) { list.Add($"Item_{i}"); }

            //string input = Console.ReadLine();


            //switch (Convert.ToInt32(input.Trim())) 
            //{
            //    case 1: Regedit.WithCount(list);
            //        break;
            //    case 2: Regedit.WithoutCount(list);
            //        break;
            //    default: throw new Exception("Invalid user input exception");
            //}

            Process.Start("https://google.com/");

            Console.ReadKey();
        }
    }
}



//try
//{
//    using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\MehSoft\UselessApp"))
//    {

//        if (key != null)
//        {
//            object o = key.GetValue("Last Open");
//            if (o != null)
//            {
//                Console.WriteLine("Last time opened on: " + o.ToString());
//            }
//            else { Console.WriteLine("Launching for the first time"); }
//        }

//        DateTime localtime = DateTime.Now;

//        Console.WriteLine("Writing new DateTime Value: " + localtime.ToString());

//        key.SetValue("Last Open", localtime);
//    }

//    using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run")) 
//    {
//        if (key != null) 
//        {
//            key.SetValue("UselessApp", Process.GetCurrentProcess().MainModule.FileName);
//        }
//    }

//}
//catch (Exception ex)
//{
//    Console.WriteLine(ex.Message);
//}