const root = document.getElementById('root')
const rightCountDoc = document.getElementById('right-answers-count')
let right = 0;

const test = [ 
    {text: 'Елизавете второй 93 года', answer: false},
    {text: 'Столица Канады - Оттава', answer: true},
    {text: 'Столица Норвегии - Берген', answer: false},
    {text: 'Родина кофе - Бразилия', answer: true},
    {text: 'Черная пантера не является самостоятельным видом', answer: true}, ]

const AddAnswerRow = (question, answer) => {

    const div = document.createElement('div');
    div.innerText = question.text
    
    if(question.answer === answer){        
        div.className = 'right question'
        right++;
    }
    else{
        div.className = 'wrong question'
    }
    root.appendChild(div);
}



let answers = []

test.forEach(question => {
    const answer = confirm(`${question.text}`)
    AddAnswerRow(question, answer)
})


