const url = 'http://www.ufa.com.ua/utilites/hdd/out.php?sort=2'
const root = document.getElementById('root')
let div = document.createElement('div')

let data = {}

data.protocol = url.split(':')[0]
data.host = url.substring(
    url.lastIndexOf('//') + 2,
    url.lastIndexOf('ua/') + 2
)
data.path = url.substring(
    url.lastIndexOf('ua/') + 2,
    url.lastIndexOf('/out')
)
data.file = url.substring(
    url.lastIndexOf('hdd/') + 4,
    url.lastIndexOf('?')
)
data.query = url.split('?')[1]

div.innerText = `${data.protocol}\n
                 ${data.host}\n
                 ${data.path}\n
                 ${data.file}\n
                 ${data.query}\n`

root.appendChild(div)
