const root = document.getElementById('root')

for(var i = 0; i < 9; i++)
{
    let row = document.createElement('div')
    row.className = 'row'
    
    for(var j = 0; j < 9; j++)
    {
        let rowelement = document.createElement('div')
        const className = (j + i) % 2 == 0 ? 'odd' : 'even'
        rowelement.className = `${className} block`;
        row.appendChild(rowelement)
    }
    root.appendChild(row);
}